# DFL::Volume

DFL::Volume provides Pipewire bindings for Qt/C++.
DFL::Volume comes with two APIs.
1. `DFL::WireAPI` or the full API. This wraps each of the pipewire structs into neat Qt/C++ classes.
It is possible to use this API to build an interface similar to pavucontrol-qt.
2. `DFL::Pipewire` or the easy API. This API uses `DFL::WireAPI` to develop a simplified `DFL::Pipewire` class.
This class is best suited to build a simple application or a plugin for volume controls.


### Dependencies:
* <tt>Qt (>= 5.15.0, or >= 6.8.0)</tt>
* <tt>canberra</tt>
* <tt>pipewire</tt>
* <tt>meson (For configuring the project)</tt>
* <tt>ninja (To build the project)</tt>


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/volume.git dfl-volume`
- Enter the `dfl-volume` folder
  * `cd dfl-volume`
- Configure the project - we use meson for project management
  * `meson setup .build --prefix=/usr --buildtype=release`
  * The above command compiles the project using Qt6.
  * Pass `-Duse_qt_version=qt5` to the above command for Qt5 build.
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know


### Upcoming
* Any feature that you ask for.


### Sample usage
```cpp
    DFL::Pipewire pw;

    QObject::connect(
        &pw, &DFL::Pipewire::defaultSinkChanged, [] ( int sinkId ) {
            // Do something here
        }
    );

    QObject::connect(
        &pw, &DFL::Pipewire::sinkVolumeChanged, [] ( qreal volume ) {
            // Do something here
        }
    );

    QObject::connect(
        &pw, &DFL::Pipewire::defaultSourceChanged, [] ( int sinkId ) {
            // Do something here
        }
    );

    QObject::connect(
        &pw, &DFL::Pipewire::sourceVolumeChanged, [] ( qreal volume ) {
            // Do something here
        }
    );

    QObject::connect(
        &pw, &DFL::Pipewire::disconnected, [&app] () {
            app.quit();
        }
    );

    pw.start();
```
