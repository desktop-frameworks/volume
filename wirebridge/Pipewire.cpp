/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "WireAPI/Pipewire.hpp"
#include <pipewire/pipewire.h>

void DFL::WireAPI::initPW( int argc, char *argv[] ) {
    pw_init( &argc, &argv );
}


void DFL::WireAPI::deinitPW() {
    pw_deinit();
}


const QString DFL::WireAPI::getApplicationName() {
    return pw_get_application_name();
}


const QString DFL::WireAPI::getProgramName() {
    return pw_get_prgname();
}


const QString DFL::WireAPI::getUserName() {
    return pw_get_user_name();
}


const QString DFL::WireAPI::getHostName() {
    return pw_get_host_name();
}


const QString DFL::WireAPI::getClientName() {
    return pw_get_client_name();
}


bool DFL::WireAPI::checkOption( const QString& option, const QString& value ) {
    return pw_check_option( option.toUtf8().constData(), value.toUtf8().constData() );
}


int DFL::WireAPI::directionReverse( int direction ) {
    return (int)pw_direction_reverse( (spa_direction)direction );
}


const QString DFL::WireAPI::getDomain() {
    return pw_get_domain();
}


int DFL::WireAPI::setDomain( const QString& domain ) {
    return pw_set_domain( domain.toUtf8().constData() );
}


uint32_t DFL::WireAPI::getSupport( struct spa_support *support, uint32_t max_support ) {
    return pw_get_support( support, max_support );
}


DFL::WireAPI::Properties DFL::WireAPI::convertSpaDictToMap( const struct spa_dict *dict ) {
    DFL::WireAPI::Properties result;

    if ( !dict ) {
        return result;
    }

    const struct spa_dict_item *item;
    spa_dict_for_each( item, dict ) {
        result.insert( QString::fromUtf8( item->key ), QString::fromUtf8( item->value ) );
    }

    return result;
}
