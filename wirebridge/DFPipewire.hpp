/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>
#include <QThread>

#include <WireAPI/Pipewire.hpp>
#include <WireAPI/MainLoop.hpp>
#include <WireAPI/Context.hpp>
#include <WireAPI/Core.hpp>
#include <WireAPI/Registry.hpp>
#include <WireAPI/Device.hpp>
#include <WireAPI/Node.hpp>
#include <WireAPI/Metadata.hpp>

namespace DFL {
    class Pipewire;
}

/**
 * This is the Easy API for pipewire.
 * While the class DFL::Pipewire uses the full WireAPI,
 * only a few events and controls are exposed to the user.
 * For a fine-grained control over all the aspects of
 * pipewire, please use the DFL::WireAPI classes
 */

class DFL::Pipewire : public QThread {
    Q_OBJECT;

    public:
        Pipewire( QObject *parent = nullptr );
        ~Pipewire();

        /** Get the volume of the device the default sink is attached to */
        qreal sinkVolume();

        /** Set the volume of the device the default sink is attached to */
        bool setSinkVolume( qreal volume );

        /** Check if the device attached to the default sink is muted */
        bool isSinkMuted();

        /** Mute/unmute the device of the default sink  */
        bool setSinkMuted( bool mute );

        /** Get the volume of the device the default source is attached to */
        qreal sourceVolume();

        /** Set the volume of the device the default source is attached to */
        bool setSourceVolume( qreal volume );

        /** Check if the device attached to the default source is muted */
        bool isSourceMuted();

        /** Mute/unmute the device of the default source  */
        bool setSourceMuted( bool mute );

        /** Get streams */
        QList<uint32_t> streams();

        /** Get the properties of a stream */
        QMap<QString, QString> streamProperties( uint32_t );

        /** Set the volume @vol of the stream @streamId */
        bool setStreamVolume( uint32_t streamId, qreal vol );

        /** Connected to pipewire server */
        Q_SIGNAL void connected();

        /**
         * Disconnected from pipewire server.
         * Delete this object as soon as the signal is emitted.
         */
        Q_SIGNAL void disconnected();

        /** Default sink changed */
        Q_SIGNAL void defaultSinkChanged( uint32_t sinkId );

        /**
         * Volume of the default sink changed.
         * This signal will be usually be emitted immediately after the
         * defaultSinkChanged signal and whenever the volume changes.
         * Use sinkId to ensure that this event is for the correct sink.
         */
        Q_SIGNAL void sinkVolumeChanged( qreal volume );

        /**
         * Mute state of the default sink changed.
         * This signal will be usually be emitted immediately after the
         * defaultSinkChanged signal and whenever the mute changes changes.
         * Use sinkId to ensure that this event is for the correct sink.
         */
        Q_SIGNAL void sinkMuteChanged( bool mute );

        /** Default source changed */
        Q_SIGNAL void defaultSourceChanged( uint32_t sinkId );

        /**
         * Volume of the default source changed.
         * This signal will be usually be emitted immediately after the
         * defaultSourceChanged signal and whenever the volume changes.
         * Use sourceId to ensure that this event is for the correct source.
         */
        Q_SIGNAL void sourceVolumeChanged( qreal volume );

        /**
         * Mute state of the default source changed.
         * This signal will be usually be emitted immediately after the
         * defaultSourceChanged signal and whenever the mute changes changes.
         * Use sourceId to ensure that this event is for the correct source.
         */
        Q_SIGNAL void sourceMuteChanged( bool mute );

        /** A new stream was added */
        Q_SIGNAL void streamAdded( uint32_t streamId );

        /** An existing stream was removed */
        Q_SIGNAL void streamRemoved( uint32_t streamId );

    private:
        DFL::WireAPI::MainLoop *mMainLoop = nullptr;
        DFL::WireAPI::Context *mCtxt      = nullptr;
        DFL::WireAPI::Core *mCore         = nullptr;
        DFL::WireAPI::Registry *mRegistry = nullptr;

        QHash<uint32_t, DFL::WireAPI::Device *> mDevices;
        QMutex mDeviceMutex;

        QHash<uint32_t, DFL::WireAPI::Node *> mSinks;
        QHash<uint32_t, DFL::WireAPI::Node *> mSources;
        QHash<uint32_t, DFL::WireAPI::Node *> mStreams;
        QMutex mNodeMutex;

        QHash<uint32_t, DFL::WireAPI::Metadata *> mMetadatas;
        QMutex mMetadataMutex;

        typedef struct route_info_t {
            QString                               name;
            uint32_t                              nodeId   = 0;
            uint32_t                              deviceId = 0;
            bool                                  ready    = false;
            DFL::WireAPI::Device::RouteProperties properties;
        } RouteInfo;

        RouteInfo inputRoute;
        RouteInfo outputRoute;

        /** Iterate through all the nodes and extract our default sink node */
        void updateDefaultSink();

        /** Iterate through all the nodes and extract our default source node */
        void updateDefaultSource();

    protected:

        /**
         * This will
         *   1. initPW()
         *   2. Create the main loop
         *   3. Create the context
         *   4. Connect core
         *   5. Create the registry
         * Additionally, it will detect if the server disconnects,
         * and call the clean up procedure for a clean exit.
         */
        void run();
};
