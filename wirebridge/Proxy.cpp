/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDebug>
#include <QVariant>

#include "WireAPI/Pipewire.hpp"
#include "WireAPI/Proxy.hpp"

#include <pipewire/pipewire.h>
#include <pipewire/core.h>
#include <pipewire/extensions/metadata.h>


DFL::WireAPI::Proxy::Proxy( struct pw_proxy *p, QObject *parent ) : DFL::WireAPI::Base( parent ), mObj( p, ProxyDeleter( this ) ) {
    // Initialize only if p is not a nullptr
    if ( p != nullptr ) {
        mListener = new struct spa_hook ( );
        mEvents   = new struct pw_proxy_events ( );

        *mEvents         = {
            .version     = PW_VERSION_PROXY_EVENTS,
            .destroy     = &DFL::WireAPI::Proxy::handleDestroy,
            .bound       = &DFL::WireAPI::Proxy::handleBound,
            .removed     = &DFL::WireAPI::Proxy::handleRemoved,
            .done        = &DFL::WireAPI::Proxy::handleDone,
            .error       = &DFL::WireAPI::Proxy::handleError,
            .bound_props = &DFL::WireAPI::Proxy::handleBoundProps,
        };

        pw_proxy_add_listener( p, mListener, mEvents, this );

        mIsValid = true;
    }
}


DFL::WireAPI::Proxy::~Proxy() {
    spa_hook_remove( mListener );
    delete mListener;
    delete mEvents;
}


void DFL::WireAPI::Proxy::initialize( struct pw_proxy *p ) {
    if ( p == nullptr ) {
        qCritical() << "Invalid pw_proxy * instance. Initialization incomplete";
        return;
    }

    if ( mObj != nullptr ) {
        qCritical() << "Initialization already done. Doing nothing";
        return;
    }

    mObj.reset( p );

    mListener = new struct spa_hook ( );
    mEvents   = new struct pw_proxy_events ( );

    *mEvents         = {
        .version     = PW_VERSION_PROXY_EVENTS,
        .destroy     = &DFL::WireAPI::Proxy::handleDestroy,
        .bound       = &DFL::WireAPI::Proxy::handleBound,
        .removed     = &DFL::WireAPI::Proxy::handleRemoved,
        .done        = &DFL::WireAPI::Proxy::handleDone,
        .error       = &DFL::WireAPI::Proxy::handleError,
        .bound_props = &DFL::WireAPI::Proxy::handleBoundProps,
    };

    pw_proxy_add_listener( p, mListener, mEvents, this );

    mIsValid = true;
}


void DFL::WireAPI::Proxy::setup() {
    if ( mObj == nullptr ) {
        qCritical() << "Initialization not done. Call initialize(...) before calling setup().";
        return;
    }

    if ( mIsSetup == false ) {
        mIsSetup = true;

        if ( mPendingRemoved ) {
            emit removed();
        }

        if ( mPendingDestroyed ) {
            emit destroyed();
        }

        if ( mPendingBound ) {
            emit bound( mPendingBound );
        }

        if ( mPendingDone ) {
            emit done( mPendingDone );
        }

        if ( mPendingError.length() ) {
            emit error( mPendingError.at( 0 ).toInt(), mPendingError.at( 1 ).toInt(), mPendingError.at( 2 ).toString() );
        }

        if ( mPendingBoundProps.length() ) {
            emit boundProps( mPendingBoundProps.at( 0 ).toInt(), mPendingBoundProps.at( 1 ).value<DFL::WireAPI::Properties>() );
        }
    }
}


bool DFL::WireAPI::Proxy::isValid() const {
    return mIsValid;
}


struct pw_proxy * DFL::WireAPI::Proxy::pwProxy() {
    return mObj.get();
}


struct pw_proxy * DFL::WireAPI::Proxy::pwProxy() const {
    return mObj.get();
}


void DFL::WireAPI::Proxy::handleDestroy( void *data ) {
    DFL::WireAPI::Proxy *proxy = reinterpret_cast<DFL::WireAPI::Proxy *>( data );

    proxy->mObj.reset( nullptr );
    proxy->mIsValid = false;

    if ( proxy && proxy->mIsSetup ) {
        emit proxy->destroyed();
    }

    else {
        proxy->mPendingDestroyed = true;
    }
}


void DFL::WireAPI::Proxy::handleBound( void *data, uint32_t globalId ) {
    DFL::WireAPI::Proxy *proxy = reinterpret_cast<DFL::WireAPI::Proxy *>( data );

    if ( proxy && proxy->mIsSetup ) {
        emit proxy->bound( globalId );
    }

    else {
        proxy->mPendingBound = globalId;
    }
}


void DFL::WireAPI::Proxy::handleRemoved( void *data ) {
    DFL::WireAPI::Proxy *proxy = reinterpret_cast<DFL::WireAPI::Proxy *>( data );

    if ( proxy && proxy->mIsSetup ) {
        emit proxy->removed();
    }

    else {
        proxy->mPendingRemoved = true;
    }

    // Destroy this
    DFL::WireAPI::Proxy::handleDestroy( data );
}


void DFL::WireAPI::Proxy::handleDone( void *data, int seq ) {
    DFL::WireAPI::Proxy *proxy = reinterpret_cast<DFL::WireAPI::Proxy *>( data );

    if ( proxy && proxy->mIsSetup ) {
        emit proxy->done( seq );
    }

    else {
        proxy->mPendingDone = seq;
    }
}


void DFL::WireAPI::Proxy::handleError( void *data, int seq, int res, const char *message ) {
    DFL::WireAPI::Proxy *proxy = reinterpret_cast<DFL::WireAPI::Proxy *>( data );

    if ( proxy && proxy->mIsSetup ) {
        emit proxy->error( seq, res, message );
    }

    else {
        proxy->mPendingError = { QVariant( seq ), QVariant( res ), QVariant( message ) };
    }
}


void DFL::WireAPI::Proxy::handleBoundProps( void *data, uint32_t globalId, const struct spa_dict *props ) {
    DFL::WireAPI::Proxy      *proxy   = reinterpret_cast<DFL::WireAPI::Proxy *>( data );
    DFL::WireAPI::Properties spaProps = DFL::WireAPI::convertSpaDictToMap( props );

    if ( proxy && proxy->mIsSetup ) {
        emit proxy->boundProps( globalId, spaProps );
    }

    else {
        proxy->mPendingBoundProps = { QVariant( globalId ), QVariant::fromValue( spaProps ) };
    }
}


/**
 * Deleter Struct
 */

DFL::WireAPI::Proxy::ProxyDeleter::ProxyDeleter( DFL::WireAPI::Proxy *instance ) : proxyInstance( instance ) {
    // Nothing else to do
}


void DFL::WireAPI::Proxy::ProxyDeleter::operator()( struct pw_proxy *pointer ) const {
    // Call the destructor only if the pointer is valid.
    if ( proxyInstance->isValid() ) {
        // Perform the destruction
        pw_proxy_destroy( pointer );

        // Set the pointer to nullptr
        pointer = nullptr;
    }
}
