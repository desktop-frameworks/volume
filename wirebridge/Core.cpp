/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDebug>
#include "WireAPI/Core.hpp"
#include <pipewire/pipewire.h>

struct spa_hook       DFL::WireAPI::Core::mListener = {};
struct pw_core_events DFL::WireAPI::Core::mEvents   = {
    .version     = PW_VERSION_CORE_EVENTS,
    .info        = &DFL::WireAPI::Core::handleInfo,
    .done        = &DFL::WireAPI::Core::handleDone,
    .ping        = &DFL::WireAPI::Core::handlePing,
    .error       = &DFL::WireAPI::Core::handleError,
    .remove_id   = &DFL::WireAPI::Core::handleRemoveId,
    .bound_id    = &DFL::WireAPI::Core::handleBoundId,
    .add_mem     = &DFL::WireAPI::Core::handleAddMem,
    .remove_mem  = &DFL::WireAPI::Core::handleRemoveMem,
    .bound_props = &DFL::WireAPI::Core::handleBoundProps,
};

DFL::WireAPI::Core::Core( const Context& ctxt, QObject *parent ) : DFL::WireAPI::Base( parent ), mObj( nullptr, CoreDeleter( this ) ) {
    // Get the underlying pw_main_loop from PWMainLoop
    struct pw_context *pwCtxt = ctxt.pwContext();

    if ( !pwCtxt ) {
        qCritical() << "Invalid pw_context * object.";
        return;
    }

    // Create the pw_core
    struct pw_core *core = pw_context_connect( pwCtxt, nullptr, 0 );

    if ( !core ) {
        qCritical() << "Failed to connect to pipewire server";
        return;
    }

    // Assign the context to the smart pointer
    mObj.reset( core );

    spa_zero( mListener );
    pw_core_add_listener( core, &mListener, &mEvents, this );

    mIsValid = true;
}


DFL::WireAPI::Core::~Core() {
    spa_hook_remove( &mListener );
}


void DFL::WireAPI::Core::setup() {
    if ( mIsSetup == false ) {
        mIsSetup = true;

        if ( mPendingInfo ) {
            emit info( mPendingInfo );
        }

        if ( std::get<0>( mPendingDone ) ) {
            emit done( std::get<1>( mPendingDone ), std::get<2>( mPendingDone ) );
        }

        if ( std::get<0>( mPendingError ) ) {
            emit error( std::get<1>( mPendingError ), std::get<2>( mPendingError ), std::get<3>( mPendingError ), std::get<4>( mPendingError ) );
        }

        if ( std::get<0>( mPendingRemoveId ) ) {
            emit idRemoved( std::get<1>( mPendingRemoveId ) );
        }

        if ( std::get<0>( mPendingBoundId ) ) {
            emit idBound( std::get<1>( mPendingBoundId ), std::get<2>( mPendingBoundId ) );
        }

        if ( std::get<0>( mPendingAddMem ) ) {
            emit memoryAdded( std::get<1>( mPendingAddMem ), std::get<2>( mPendingAddMem ), std::get<3>( mPendingAddMem ), std::get<4>( mPendingAddMem ) );
        }

        if ( std::get<0>( mPendingRemoveMem ) ) {
            emit memoryRemoved( std::get<1>( mPendingRemoveMem ) );
        }

        if ( std::get<0>( mPendingBoundProps ) ) {
            emit boundProps( std::get<1>( mPendingBoundProps ), std::get<2>( mPendingBoundProps ), std::get<3>( mPendingBoundProps ) );
        }
    }
}


bool DFL::WireAPI::Core::isValid() const {
    return mIsValid;
}


struct pw_core * DFL::WireAPI::Core::pwCore() {
    return mObj.get();
}


struct pw_core * DFL::WireAPI::Core::pwCore() const {
    return mObj.get();
}


void DFL::WireAPI::Core::handleInfo( void *data, const struct pw_core_info *info ) {
    DFL::WireAPI::Core *core = reinterpret_cast<DFL::WireAPI::Core *>( data );

    if ( core && core->mIsSetup ) {
        emit core->info( info );
    }

    else {
        core->mPendingInfo = info;
    }
}


void DFL::WireAPI::Core::handleDone( void *data, uint32_t id, int seq ) {
    DFL::WireAPI::Core *core = reinterpret_cast<DFL::WireAPI::Core *>( data );

    if ( core && core->mIsSetup ) {
        emit core->done( id, seq );
    }

    else {
        core->mPendingDone = { true, id, seq };
    }
}


void DFL::WireAPI::Core::handlePing( void *data, uint32_t id, int seq ) {
    DFL::WireAPI::Core *core = reinterpret_cast<DFL::WireAPI::Core *>( data );

    if ( core ) {
        pw_core_pong( core->mObj.get(), id, seq );
    }
}


void DFL::WireAPI::Core::handleError( void *data, uint32_t id, int seq, int res, const char *message ) {
    DFL::WireAPI::Core *core = reinterpret_cast<DFL::WireAPI::Core *>( data );

    if ( core && core->mIsSetup ) {
        emit core->error( id, seq, res, message );

        /** Server disconnected */
        if ( ( id == PW_ID_CORE ) && ( res == -EPIPE ) ) {
            core->mIsValid = false;
        }
    }

    else {
        core->mPendingError = { true, id, seq, res, message };
    }
}


void DFL::WireAPI::Core::handleRemoveId( void *data, uint32_t id ) {
    DFL::WireAPI::Core *core = reinterpret_cast<DFL::WireAPI::Core *>( data );

    if ( core && core->mIsSetup ) {
        emit core->idRemoved( id );
    }

    else {
        core->mPendingRemoveId = { true, id };
    }
}


void DFL::WireAPI::Core::handleBoundId( void *data, uint32_t id, uint32_t global_id ) {
    DFL::WireAPI::Core *core = reinterpret_cast<DFL::WireAPI::Core *>( data );

    if ( core && core->mIsSetup ) {
        emit core->idBound( id, global_id );
    }

    else {
        core->mPendingBoundId = { true, id, global_id };
    }
}


void DFL::WireAPI::Core::handleAddMem( void *data, uint32_t id, uint32_t type, int fd, uint32_t flags ) {
    DFL::WireAPI::Core *core = reinterpret_cast<DFL::WireAPI::Core *>( data );

    if ( core && core->mIsSetup ) {
        emit core->memoryAdded( id, type, fd, flags );
    }

    else {
        core->mPendingAddMem = { true, id, type, fd, flags };
    }
}


void DFL::WireAPI::Core::handleRemoveMem( void *data, uint32_t id ) {
    DFL::WireAPI::Core *core = reinterpret_cast<DFL::WireAPI::Core *>( data );

    if ( core && core->mIsSetup ) {
        emit core->memoryRemoved( id );
    }

    else {
        core->mPendingRemoveMem = { true, id };
    }
}


void DFL::WireAPI::Core::handleBoundProps( void *data, uint32_t id, uint32_t global_id, const struct spa_dict *props ) {
    DFL::WireAPI::Core *core = reinterpret_cast<DFL::WireAPI::Core *>( data );

    if ( core && core->mIsSetup ) {
        emit core->boundProps( id, global_id, props );
    }

    else {
        core->mPendingBoundProps = { true, id, global_id, props };
    }
}


/**
 * Deleter Struct
 */

DFL::WireAPI::Core::CoreDeleter::CoreDeleter( DFL::WireAPI::Core *instance ) : coreInstance( instance ) {
    // Nothing else to do
}


void DFL::WireAPI::Core::CoreDeleter::operator()( struct pw_core *pointer ) const {
    /** Disconnect */
    pw_core_disconnect( pointer );

    // Call the destructor only if the pointer is valid.
    if ( coreInstance->isValid() ) {
        // Perform the destruction
        pw_proxy_destroy( (struct pw_proxy *)pointer );

        // Set the pointer to nullptr
        pointer = nullptr;
    }
}
