/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>
#include <QThread>
#include <QDebug>

#include <spa/utils/hook.h>
#include <spa/utils/list.h>
#include <pipewire/context.h>
#include <pipewire/pipewire.h>

namespace DFL {
    namespace PipeAPI {
        class Monitor;
        class Registry;
        class Device;
        class Node;
        class Port;

        typedef QMap<QString, QString> Properties;
    }
}

class DFL::PipeAPI::Monitor : public QThread {
    Q_OBJECT;

    public:
        Monitor( QObject *parent = nullptr );
        ~Monitor();

        /** Init pipewire */
        void init( int, char ** );

        /** Cleanup the resources (typically after crash) */
        void cleanup();

        Registry *getRegistry();

        pw_main_loop *mainLoop();
        pw_context *context();
        pw_core *core();
        pw_registry *registry();

        Node * defaultSink();
        Node * defaultSource();

        Q_SIGNAL void deviceAdded( Device * );

        Q_SIGNAL void sinkAdded( Node * );
        Q_SIGNAL void sourceAdded( Node * );
        Q_SIGNAL void streamAdded( Node * );

        Q_SIGNAL void defaultSinkChanged();
        Q_SIGNAL void defaultSourceChanged();


    protected:
        void run();

    signals:
        void clientConnected( uint32_t clientId );
        void sinkAvailable( uint32_t sinkId );
        void sourceAvailable( uint32_t sourceId );
        void streamAvailable( uint32_t streamId );

        void serverConnected();
        void serverDisconnected();

    private:
        struct pw_main_loop *mLoop = nullptr;

        struct pw_context *mCtxt = nullptr;
        struct spa_hook contextListener;

        struct pw_core *mCore = nullptr;
        struct spa_hook coreListener;

        struct pw_registry *mRegistry = nullptr;
        struct spa_hook registryListener;

        QHash<uint32_t, DFL::PipeAPI::Device *> mDevices;

        QHash<uint32_t, DFL::PipeAPI::Node *> mSinks;
        QHash<uint32_t, DFL::PipeAPI::Node *> mSources;
        QHash<uint32_t, DFL::PipeAPI::Node *> mStreams;

        QHash<uint32_t, uint32_t> mPortNodeMap;

        uint32_t defaultSinkId   = 0;
        uint32_t defaultSourceId = 0;

        static void coreEventError( void *data, uint32_t id, int seq, int res, const char *message );

        static void registryEventGlobal( void *data, uint32_t id, uint32_t permissions, const char *type, uint32_t version, const struct spa_dict *props );
        static void registryEventGlobalRemove( void *data, uint32_t id );

        void updateDefaultSinkId();
        void updateDefaultSourceId();
};

class DFL::PipeAPI::Device : public QObject {
    Q_OBJECT;

    public:
        Device( struct pw_device *, Properties props, uint32_t id );
        ~Device();

        struct pw_device *get();

        uint32_t globalId();

        Properties properties();

        /** Volume getter/setter */
        qreal volume();
        bool setVolume( qreal );

        /** Mute getter/setter */
        bool isMuted();
        bool setMuted( bool );

        Q_SIGNAL void parametersChanged();
        Q_SIGNAL void propertiesChanged();

    private:
        uint32_t mGlobalId        = 0;
        struct pw_device *mDevice = nullptr;
        struct spa_hook deviceListener;

        Properties mProperties;

        static void deviceEventInfo( void *data, const struct pw_device_info *info );
        static void deviceEventParam( void *data, int seq, uint32_t id, uint32_t index, uint32_t next, const struct spa_pod *param );
};


class DFL::PipeAPI::Node : public QObject {
    Q_OBJECT;

    public:
        enum class Type {
            Sink,
            Source,
            Stream,
        };

        enum class State {
            Error     = PW_NODE_STATE_ERROR,
            Creating  = PW_NODE_STATE_CREATING,
            Suspended = PW_NODE_STATE_SUSPENDED,
            Idle      = PW_NODE_STATE_IDLE,
            Running   = PW_NODE_STATE_RUNNING,
        };

        Node( struct pw_node *, Properties props, uint32_t id );
        ~Node();

        struct pw_node *get();

        uint32_t globalId();

        Properties properties();

        State state();
        QString error();

        /** Volume getter/setter */
        qreal volume();
        bool setVolume( qreal );

        /** Mute getter/setter */
        bool isMuted();
        bool setMuted( bool );

        void addPort( Port * );
        void removePort( uint32_t );
        bool hasPort( uint32_t );

        Q_SIGNAL void stateChanged();

        Q_SIGNAL void parametersChanged();
        Q_SIGNAL void propertiesChanged();

        Q_SIGNAL void portAdded( Port * );
        Q_SIGNAL void portRemoved( uint32_t );
        Q_SIGNAL void defaultPortChanged();

    private:
        uint32_t mGlobalId    = 0;
        struct pw_node *mNode = nullptr;
        struct spa_hook nodeListener;

        Properties mProperties;

        State mState;
        QString mError;

        QHash<uint32_t, Port *> mPorts;

        static void nodeEventInfo( void *data, const struct pw_node_info *info );
        static void nodeEventParam( void *data, int seq, uint32_t id, uint32_t index, uint32_t next, const struct spa_pod *param );
};


class DFL::PipeAPI::Port : public QObject {
    Q_OBJECT;

    public:
        Port( struct pw_port *, Properties props, uint32_t id );
        ~Port();

        struct pw_port *get();

        uint32_t globalId();

        Properties properties();

        /** Volume getter/setter */
        qreal volume();
        bool setVolume( qreal );

        /** Mute getter/setter */
        bool isMuted();
        bool setMuted( bool );

        Q_SIGNAL void stateChanged();

        Q_SIGNAL void parametersChanged();
        Q_SIGNAL void propertiesChanged();

        Q_SIGNAL void portAdded( Port * );
        Q_SIGNAL void portremoved( Port * );
        Q_SIGNAL void defaultPortChanged();

    private:
        uint32_t mGlobalId    = 0;
        struct pw_port *mPort = nullptr;
        struct spa_hook portListener;

        Properties mProperties;

        static void portEventInfo( void *data, const struct pw_port_info *info );
        static void portEventParam( void *data, int seq, uint32_t id, uint32_t index, uint32_t next, const struct spa_pod *param );
};
