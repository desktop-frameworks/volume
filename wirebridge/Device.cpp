/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <iostream>
#include <numeric>

#include <QDebug>
#include <QEventLoop>

#include "WireAPI/Device.hpp"

#include <pipewire/pipewire.h>
#include <spa/param/props-types.h>
#include <spa/param/props.h>
#include <spa/param/route-types.h>
#include <spa/param/route.h>
#include <spa/pod/builder.h>
#include <spa/pod/parser.h>
#include <spa/pod/iter.h>
#include <spa/pod/pod.h>

#include <spa/utils/result.h>
#include <spa/utils/string.h>
#include <spa/debug/pod.h>
#include <spa/utils/keys.h>
#include <spa/utils/json-pod.h>
#include <spa/pod/dynamic.h>


static inline QList<qreal> readFloatArray( const struct spa_pod *array_pod ) {
    // Initialize a parser for the array POD
    struct spa_pod_parser parser;

    spa_pod_parser_pod( &parser, array_pod );

    // Parse array header
    uint32_t element_size, element_type, array_size;
    void     *array_data;

    if ( spa_pod_parser_get( &parser, SPA_POD_Array( &element_size, &element_type, &array_size, &array_data ), NULL ) < 0 ) {
        qWarning() << "Failed to parse array";
        return QList<qreal>();
    }

    QList<qreal> floats;

    if ( element_type == SPA_TYPE_Float ) {
        float *values = (float *)array_data;
        for (uint32_t i = 0; i < array_size; i++) {
            /**
             * Pipewire gives us cubed values.
             * The actual value is cube-root of the pw-volume.
             */
            floats << pow( values[ i ], 1.0 / 3.0 );
        }
    }

    return floats;
}


static inline QStringList readStringArray( const struct spa_pod *array_pod ) {
    // Initialize a parser for the array POD
    struct spa_pod_parser parser;

    spa_pod_parser_pod( &parser, array_pod );

    // Parse array header
    uint32_t element_size, element_type, array_size;
    void     *array_data;

    if ( spa_pod_parser_get( &parser, SPA_POD_Array( &element_size, &element_type, &array_size, &array_data ), NULL ) < 0 ) {
        qWarning() << "Failed to parse array";
        return QStringList();
    }

    QStringList strings;

    if ( element_type == SPA_TYPE_String ) {
        // Cast the array data to a pointer to char* (array of strings)
        const char **values = (const char **)array_data;

        // Iterate over the array and extract each string
        for (uint32_t i = 0; i < array_size; i++) {
            if ( values[ i ] != nullptr ) {
                strings << QString::fromUtf8( values[ i ] ); // Convert to QString
            }
            else {
                strings << QString(); // Handle null strings if necessary
            }
        }
    }

    return strings;
}


bool operator==( const DFL::WireAPI::Device::RouteProperties& lhs, const DFL::WireAPI::Device::RouteProperties& rhs ) {
    return lhs.name == rhs.name &&
           lhs.description == rhs.description &&
           lhs.id == rhs.id &&
           lhs.direction == rhs.direction &&
           lhs.priority == rhs.priority &&
           lhs.device == rhs.device &&
           qFuzzyCompare( lhs.volume, rhs.volume ) && // Compare floating-point values safely
           lhs.channelVolumes == rhs.channelVolumes &&
           lhs.channelNames == rhs.channelNames &&
           lhs.isMuted == rhs.isMuted;
}


bool operator!=( const DFL::WireAPI::Device::RouteProperties& lhs, const DFL::WireAPI::Device::RouteProperties& rhs ) {
    return lhs.name != rhs.name ||
           lhs.description != rhs.description ||
           lhs.id != rhs.id ||
           lhs.direction != rhs.direction ||
           lhs.priority != rhs.priority ||
           lhs.device != rhs.device ||
           !qFuzzyCompare( lhs.volume, rhs.volume ) || // Compare floating-point values safely
           lhs.channelVolumes != rhs.channelVolumes ||
           lhs.channelNames != rhs.channelNames ||
           lhs.isMuted != rhs.isMuted;
}


DFL::WireAPI::Device::Device( struct pw_device *device, Properties props, uint32_t id, QObject *parent ) : DFL::WireAPI::Proxy( reinterpret_cast<pw_proxy *>( device ), parent ) {
    mObj      = device;
    mGlobalId = id;

    mProperties = props;

    // Allocate memory for the listener and events struct
    mListener     = new spa_hook();
    mDeviceEvents = new pw_device_events();

    *mDeviceEvents = {
        .version   = PW_VERSION_DEVICE_EVENTS,
        .info      = deviceEventInfo,
        .param     = deviceEventParam,
    };

    spa_zero( *mListener );
    pw_device_add_listener( mObj, mListener, mDeviceEvents, this );

    // Define the parameter IDs to subscribe to
    uint32_t ids[] = { SPA_PARAM_Props, SPA_PARAM_Route };

    // Subscribe to these parameters
    pw_device_subscribe_params( mObj, ids, 2 );
}


DFL::WireAPI::Device::~Device() {
    spa_hook_remove( mListener );
    delete mListener;
    delete mDeviceEvents;
}


void DFL::WireAPI::Device::setup() {
    if ( mIsSetup == false ) {
        // Call setup() of parent class Proxy
        // Events of Proxy will be emitted here.
        Proxy::setup();

        if ( mPendingProperties ) {
            emit propertiesChanged();
        }

        if ( mPendingParameters ) {
            emit parametersChanged();
        }
    }
}


struct pw_device *DFL::WireAPI::Device::pwDevice() {
    return mObj;
}


struct pw_device *DFL::WireAPI::Device::pwDevice() const {
    return mObj;
}


uint32_t DFL::WireAPI::Device::globalId() {
    return mGlobalId;
}


DFL::WireAPI::Properties DFL::WireAPI::Device::properties() {
    return mProperties;
}


qreal DFL::WireAPI::Device::sinkVolume() {
    return mOutputProps.volume;
}


bool DFL::WireAPI::Device::setSinkVolume( qreal volume ) {
    /** Sink is not ready yet */
    if ( mOutputProps.id == -1 ) {
        return false;
    }

    float   volCube = (float)( volume * volume * volume );
    QString json    = QString( "{index: %1, device: %2, props: {volume: %3}}" ).arg( mOutputProps.id ).arg( mOutputProps.device ).arg( volCube, 0, 'f', 6 );

    return setRouteParameter( json );
}


qreal DFL::WireAPI::Device::sourceVolume() {
    return mInputProps.volume;
}


bool DFL::WireAPI::Device::setSourceVolume( qreal volume ) {
    /** Source is not ready yet */
    if ( mInputProps.id == -1 ) {
        return false;
    }

    float   volCube = (float)( volume * volume * volume );
    QString json    = QString( "{index: %1, device: %2, props: {volume: %3}}" ).arg( mInputProps.id ).arg( mInputProps.device ).arg( volCube, 0, 'f', 6 );

    return setRouteParameter( json );
}


bool DFL::WireAPI::Device::isSinkMuted() {
    return mOutputProps.isMuted;
}


bool DFL::WireAPI::Device::setSinkMuted( bool mute ) {
    /** Sink is not ready yet */
    if ( mOutputProps.id == -1 ) {
        return false;
    }

    QString json = QString( "{index: %1, device: %2, props: {mute: %3}}" ).arg( mOutputProps.id ).arg( mOutputProps.device ).arg( mute ? "true" : "false" );

    return setRouteParameter( json );
}


bool DFL::WireAPI::Device::isSourceMuted() {
    return mInputProps.isMuted;
}


bool DFL::WireAPI::Device::setSourceMuted( bool mute ) {
    /** Source is not ready yet */
    if ( mInputProps.id == -1 ) {
        return false;
    }

    QString json = QString( "{index: %1, device: %2, props: {mute: %3}}" ).arg( mInputProps.id ).arg( mInputProps.device ).arg( mute ? "true" : "false" );

    return setRouteParameter( json );
}


void DFL::WireAPI::Device::deviceEventInfo( void *data, const struct pw_device_info *info ) {
    // Check for null pointers
    if ( !data || !info ) {
        qWarning() << "deviceEventInfo: data or info is null";
        return;
    }

    auto *device = static_cast<DFL::WireAPI::Device *>( data );

    // Ensure thread-safe access to device data
    QMutexLocker locker( &device->mMutex );

    if ( info->change_mask & PW_DEVICE_CHANGE_MASK_PROPS ) {
        /** Merge the existing with the new ones */
        DFL::WireAPI::Properties newProps = convertSpaDictToMap( info->props );
        device->mProperties.insert( newProps );

        if ( device->mIsSetup ) {
            emit device->propertiesChanged();
        }

        else {
            device->mPendingProperties = true;
        }
    }

    if ( info->change_mask & PW_DEVICE_CHANGE_MASK_PARAMS ) {
        if ( device->mIsSetup ) {
            emit device->parametersChanged();

            // Let's device a call to update the parameters
            pw_device_enum_params( device->mObj, 0, SPA_PARAM_Route, 0, 0, nullptr );
        }

        else {
            device->mPendingParameters = true;
        }
    }
}


void DFL::WireAPI::Device::deviceEventParam( void *data, int, uint32_t id, uint32_t, uint32_t, const struct spa_pod *param ) {
    if ( !param ) {
        qWarning() << "deviceEventParam: Received a null parameter.";
        return;
    }

    auto *device = static_cast<DFL::WireAPI::Device *>( data );

    if ( id == SPA_PARAM_Props ) {
        if ( spa_pod_is_object( param ) ) {
            struct spa_pod_object *obj = (struct spa_pod_object *)param;

            // Iterate through the properties in the object
            struct spa_pod_prop *iter;
            for ( iter = spa_pod_prop_first( &( obj->body ) ); spa_pod_prop_is_inside( &( obj->body ), param->size, iter ); iter = spa_pod_prop_next( iter ) ) {
                if ( iter->key == SPA_PROP_volume ) {
                    float volume = 0;
                    spa_pod_get_float( &( iter->value ), &volume );
                    device->mProperties[ "volume" ] = QString::number( volume );
                }

                else if ( iter->key == SPA_PROP_mute ) {
                    bool mute = false;
                    spa_pod_get_bool( &( iter->value ), &mute );
                    device->mProperties[ "mute" ] = ( mute ? "true" : "false" );
                }

                else {
                }
            }
        }
    }

    else if ( id == SPA_PARAM_Route ) {
        if ( spa_pod_is_object( param ) ) {
            struct spa_pod_object *obj = (struct spa_pod_object *)param;

            RouteProperties newProps;

            // Iterate through the properties in the object
            struct spa_pod_prop *iter;
            for ( iter = spa_pod_prop_first( &( obj->body ) ); spa_pod_prop_is_inside( &( obj->body ), param->size, iter ); iter = spa_pod_prop_next( iter ) ) {
                switch ( iter->key ) {
                    case SPA_PARAM_ROUTE_index: {
                        if ( spa_pod_get_int( &( iter->value ), &newProps.id ) >= 0 ) {
                            // Nothing to do
                        }

                        break;
                    }

                    case SPA_PARAM_ROUTE_direction: {
                        uint32_t value = 0;

                        if ( spa_pod_get_id( &( iter->value ), &value ) >= 0 ) {
                            newProps.direction = (int)value;
                        }

                        break;
                    }

                    case SPA_PARAM_ROUTE_device: {
                        int value = 0;

                        if ( spa_pod_get_int( &( iter->value ), &value ) >= 0 ) {
                            newProps.device = value;
                        }

                        break;
                    }

                    case SPA_PARAM_ROUTE_name: {
                        const char *value = { 0 };

                        if ( spa_pod_get_string( &( iter->value ), &value ) >= 0 ) {
                            newProps.name = value;
                        }

                        break;
                    }

                    case SPA_PARAM_ROUTE_description: {
                        const char *value = { 0 };

                        if ( spa_pod_get_string( &( iter->value ), &value ) >= 0 ) {
                            newProps.description = value;
                        }

                        break;
                    }

                    case SPA_PARAM_ROUTE_priority: {
                        int value = 0;

                        if ( spa_pod_get_int( &( iter->value ), &value ) >= 0 ) {
                            newProps.priority = value;
                        }

                        break;
                    }

                    case SPA_PARAM_ROUTE_available: {
                        uint32_t value = 0;

                        if ( spa_pod_get_id( &( iter->value ), &value ) >= 0 ) {
                            // We're not bothered about this at the moment
                        }

                        break;
                    }

                    case SPA_PARAM_ROUTE_info: {
                        // We currently don;t know how to retrieve this
                        // int value = 0;
                        // spa_pod_get_int( &(iter->value), &value );

                        break;
                    }

                    case SPA_PARAM_ROUTE_profiles: {
                        int value = 0;

                        if ( spa_pod_get_int( &( iter->value ), &value ) >= 0 ) {
                            // Perhaps at a later stage!
                        }

                        break;
                    }

                    case SPA_PARAM_ROUTE_props: {
                        struct spa_pod *props_pod = &iter->value;

                        // Cast the value to a spa_pod_object
                        const struct spa_pod_object *props_obj = (const struct spa_pod_object *)props_pod;

                        // Iterate through the properties in the object
                        struct spa_pod_prop *prop_iter;
                        for (prop_iter = spa_pod_prop_first( &props_obj->body );
                             spa_pod_prop_is_inside( &props_obj->body, props_obj->pod.size, prop_iter );
                             prop_iter = spa_pod_prop_next( prop_iter ) ) {
                            // Parse the value based on its type
                            switch ( prop_iter->key ) {
                                case SPA_PROP_channelVolumes: {
                                    newProps.channelVolumes = readFloatArray( &prop_iter->value );
                                    break;
                                }

                                case SPA_PROP_channelMap: {
                                    newProps.channelNames = readStringArray( &prop_iter->value );
                                    break;
                                }

                                case SPA_PROP_mute: {
                                    spa_pod_get_bool( &prop_iter->value, &newProps.isMuted );
                                    break;
                                }

                                default: {
                                    break;
                                }
                            }
                        }

                        break;
                    }

                    case SPA_PARAM_ROUTE_devices: {
                        // int value = 0;
                        // spa_pod_get_int( &( iter->value ), &value );

                        break;
                    }

                    case SPA_PARAM_ROUTE_profile: {
                        int value = 0;
                        spa_pod_get_int( &( iter->value ), &value );

                        break;
                    }

                    case SPA_PARAM_ROUTE_save: {
                        bool value = 0;
                        spa_pod_get_bool( &( iter->value ), &value );

                        break;
                    }

                    default: {
                        break;
                    }
                }
            }

            if ( newProps.direction != -1 ) {
                newProps.volume = std::reduce( newProps.channelVolumes.begin(), newProps.channelVolumes.end(), 0.0f ) / newProps.channelVolumes.length();

                if ( newProps.direction == 0 ) {
                    /** Change in priority: New priority is higher; replace */
                    if ( newProps.priority > device->mInputProps.priority ) {
                        device->mInputProps = newProps;

                        emit device->defaultRouteChanged( device->mInputProps.direction );
                        emit device->sourcePropertiesChanged( newProps );
                    }

                    /** Same route: probably some property changed */
                    if ( ( newProps.id == device->mInputProps.id ) && ( newProps.priority == device->mInputProps.priority ) ) {
                        if ( device->mInputProps != newProps ) {
                            device->mInputProps = newProps;
                            emit device->sourcePropertiesChanged( newProps );
                        }
                    }
                }

                else if ( newProps.direction == 1 ) {
                    /** Change in priority: New priority is higher; replace */
                    if ( newProps.priority > device->mOutputProps.priority ) {
                        device->mOutputProps = newProps;

                        emit device->defaultRouteChanged( device->mOutputProps.direction );
                        emit device->sinkPropertiesChanged( newProps );
                    }

                    /** Same route: probably some property changed */
                    if ( ( newProps.id == device->mOutputProps.id ) && ( newProps.priority == device->mOutputProps.priority ) ) {
                        if ( device->mOutputProps != newProps ) {
                            device->mOutputProps = newProps;
                            emit device->sinkPropertiesChanged( newProps );
                        }
                    }
                }
            }
        }
    }
}


bool DFL::WireAPI::Device::setRouteParameter( QString json ) {
    uint8_t buffer[ 1024 ];

    spa_auto( spa_pod_dynamic_builder ) builder = { 0 };
    const struct spa_type_info *typeInfo = nullptr;
    struct spa_pod             *pod      = nullptr;
    uint32_t paramId;

    // Initialize the SPA POD dynamic builder
    spa_pod_dynamic_builder_init( &builder, buffer, sizeof( buffer ), 4096 );

    // Find the SPA type info for the parameter (in this case, SPA_PARAM_Route)
    typeInfo = spa_debug_type_find_short( spa_type_param, "Route" );

    if ( typeInfo == nullptr ) {
        qCritical() << "Unknown param type: Route";
        return false;
    }

    // Convert the JSON string to a SPA POD
    int res = spa_json_to_pod( &builder.b, 0, typeInfo, json.toUtf8().constData(), json.length() );

    if ( res < 0 ) {
        qCritical() << "Can't make pod:" << spa_strerror( res );
        return false;
    }

    // Dereference the built SPA POD
    if ( ( pod = spa_pod_builder_deref( &builder.b, 0 ) ) == NULL ) {
        qCritical() << "Can't make pod";
        return false;
    }

    // Debugging: Print the constructed SPA POD
    spa_debug_pod( 0, NULL, pod );

    // Set the parameter ID to SPA_PARAM_Route
    paramId = typeInfo->type;

    // Set the parameter on the device
    res = pw_device_set_param( mObj, paramId, 0, pod );

    if ( res < 0 ) {
        qCritical() << "Failed to set parameter:" << spa_strerror( res );
        return false;
    }

    return true;
}
