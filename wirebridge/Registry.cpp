/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDebug>
#include <QVariant>

#include "WireAPI/Core.hpp"
#include "WireAPI/Registry.hpp"
#include "WireAPI/Pipewire.hpp"

#include <pipewire/pipewire.h>
#include <pipewire/core.h>
#include <pipewire/extensions/metadata.h>


DFL::WireAPI::Registry::Registry( const Core& core, QObject *parent ) : DFL::WireAPI::Proxy( nullptr, parent ), mObj( nullptr ) {
    // Get the underlying pw_main_loop from PWMainLoop
    struct pw_core *pwCore = core.pwCore();

    if ( !pwCore ) {
        qCritical() << "Invalid pw_core * object.";
        return;
    }

    // Create the pw_registry
    struct pw_registry *registry = pw_core_get_registry( pwCore, PW_VERSION_REGISTRY, 0 );

    if ( !registry ) {
        qCritical() << "Failed to create the registry";
        return;
    }

    // Save the raw pointer since the underlying pw_proxy is a smart pointer.
    mObj = registry;

    // Registry listeners
    mListener = new struct spa_hook ( );
    mEvents   = new struct pw_registry_events ( );

    *mEvents           = {
        .version       = PW_VERSION_REGISTRY_EVENTS,
        .global        = handleGlobal,
        .global_remove = handleGlobalRemove,
    };

    pw_registry_add_listener( registry, mListener, mEvents, this );

    // Deferred initialization of pw_proxy smart pointer.
    initialize( reinterpret_cast<struct pw_proxy *>( mObj ) );

    // All good. This object is now valid.
    mIsValid = true;
}


DFL::WireAPI::Registry::~Registry() {
    spa_hook_remove( mListener );
    delete mListener;
    delete mEvents;
}


void DFL::WireAPI::Registry::setup() {
    if ( mIsSetup == false ) {
        // Call setup() of parent class Proxy
        // Events of Proxy will be emitted here.
        Proxy::setup();

        // Emit the events of Registry.
        while ( mPending.count() ) {
            QVariantList contents = mPending.take( mPending.keys().first() );
            uint32_t     id       = contents[ 0 ].toUInt();
            QString      type     = contents[ 1 ].toString();
            uint32_t     version  = contents[ 2 ].toUInt();
            Properties   props    = contents[ 3 ].value<DFL::WireAPI::Properties>();

            if ( type == PW_TYPE_INTERFACE_Device ) {
                emit deviceAdded( id, version, props );
            }

            else if ( type == PW_TYPE_INTERFACE_Node ) {
                emit nodeAdded( id, version, props );
            }

            else if ( type == PW_TYPE_INTERFACE_Port ) {
                emit portAdded( id, version, props );
            }

            else if ( type == PW_TYPE_INTERFACE_Metadata ) {
                emit metadataAdded( id, version, props );
            }

            else {
                emit globalAdded( id, version, type, props );
            }
        }
    }
}


bool DFL::WireAPI::Registry::isValid() const {
    return mIsValid;
}


struct pw_registry * DFL::WireAPI::Registry::pwRegistry() {
    return mObj;
}


struct pw_registry * DFL::WireAPI::Registry::pwRegistry() const {
    return mObj;
}


struct pw_device * DFL::WireAPI::Registry::bindDevice( uint32_t id ) {
    return reinterpret_cast<struct pw_device *>( pw_registry_bind( mObj, id, PW_TYPE_INTERFACE_Device, PW_VERSION_DEVICE, 0 ) );
}


struct pw_node * DFL::WireAPI::Registry::bindNode( uint32_t id ) {
    return reinterpret_cast<struct pw_node *>( pw_registry_bind( mObj, id, PW_TYPE_INTERFACE_Node, PW_VERSION_NODE, 0 ) );
}


struct pw_port * DFL::WireAPI::Registry::bindPort( uint32_t id ) {
    return reinterpret_cast<struct pw_port *>( pw_registry_bind( mObj, id, PW_TYPE_INTERFACE_Port, PW_VERSION_PORT, 0 ) );
}


struct pw_metadata * DFL::WireAPI::Registry::bindMetadata( uint32_t id ) {
    return reinterpret_cast<struct pw_metadata *>( pw_registry_bind( mObj, id, PW_TYPE_INTERFACE_Metadata, PW_VERSION_METADATA, 0 ) );
}


void * DFL::WireAPI::Registry::bindGlobal( uint32_t id, QString type, uint32_t version ) {
    return pw_registry_bind( mObj, id, type.toUtf8().constData(), version, 0 );
}


void DFL::WireAPI::Registry::handleGlobal( void *data, uint32_t id, uint32_t, const char *type, uint32_t version, const struct spa_dict *spaProps ) {
    DFL::WireAPI::Registry *registry = reinterpret_cast<DFL::WireAPI::Registry *>( data );

    Properties props = DFL::WireAPI::convertSpaDictToMap( spaProps );

    if ( registry ) {
        if ( strcmp( type, PW_TYPE_INTERFACE_Device ) == 0 ) {
            if ( registry->mIsSetup ) {
                emit registry->deviceAdded( id, version, props );
            }

            else {
                registry->mPending[ id ] = QVariantList() << id << QString( type ) << version << QVariant::fromValue( props );
            }
        }

        else if ( strcmp( type, PW_TYPE_INTERFACE_Node ) == 0 ) {
            if ( registry->mIsSetup ) {
                emit registry->nodeAdded( id, version, props );
            }

            else {
                registry->mPending[ id ] = QVariantList() << id << QString( type ) << version << QVariant::fromValue( props );
            }
        }

        else if ( strcmp( type, PW_TYPE_INTERFACE_Port ) == 0 ) {
            if ( registry->mIsSetup ) {
                emit registry->portAdded( id, version, props );
            }

            else {
                registry->mPending[ id ] = QVariantList() << id << QString( type ) << version << QVariant::fromValue( props );
            }
        }

        else if ( strcmp( type, PW_TYPE_INTERFACE_Metadata ) == 0 ) {
            if ( registry->mIsSetup ) {
                emit registry->metadataAdded( id, version, props );
            }

            else {
                registry->mPending[ id ] = QVariantList() << id << QString( type ) << version << QVariant::fromValue( props );
            }
        }

        else {
            if ( registry->mIsSetup ) {
                emit registry->globalAdded( id, version, QString( type ), props );
            }

            else {
                registry->mPending[ id ] = QVariantList() << id << QString( type ) << version << QVariant::fromValue( props );
            }
        }
    }
}


void DFL::WireAPI::Registry::handleGlobalRemove( void *data, uint32_t id ) {
    DFL::WireAPI::Registry *registry = reinterpret_cast<DFL::WireAPI::Registry *>( data );

    if ( registry && registry->mIsSetup ) {
        emit registry->globalRemoved( id );
    }

    else {
        registry->mPending.remove( id );
    }
}
