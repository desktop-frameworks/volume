/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDebug>
#include <QVariant>
#include <QVariantList>
#include <QEventLoop>
#include <QTimer>

#include <QJsonDocument>
#include <QJsonObject>

#include "DFPipewire.hpp"

#include <pipewire/pipewire.h>

DFL::Pipewire::Pipewire( QObject *parent ) : QThread( parent ) {
    // qInfo() << "Creating DFL::Pipewire instance.";
}


DFL::Pipewire::~Pipewire() {
    if ( mMainLoop ) {
        delete mMainLoop;
    }

    if ( mCtxt ) {
        delete mCtxt;
    }

    if ( mCore ) {
        delete mCore;
    }

    if ( mRegistry ) {
        delete mRegistry;
    }

    qDeleteAll( mStreams );
}


qreal DFL::Pipewire::sinkVolume() {
    if ( mDevices.contains( outputRoute.deviceId ) ) {
        return mDevices[ outputRoute.deviceId ]->sinkVolume();
    }

    return -1.0;
}


bool DFL::Pipewire::setSinkVolume( qreal volume ) {
    if ( mDevices.contains( outputRoute.deviceId ) ) {
        return mDevices[ outputRoute.deviceId ]->setSinkVolume( volume );
    }

    return false;
}


bool DFL::Pipewire::isSinkMuted() {
    if ( mDevices.contains( outputRoute.deviceId ) ) {
        return mDevices[ outputRoute.deviceId ]->isSinkMuted();
    }

    return false;
}


bool DFL::Pipewire::setSinkMuted( bool mute ) {
    if ( mDevices.contains( outputRoute.deviceId ) ) {
        return mDevices[ outputRoute.deviceId ]->setSinkMuted( mute );
    }

    return false;
}


qreal DFL::Pipewire::sourceVolume() {
    if ( mDevices.contains( inputRoute.deviceId ) ) {
        return mDevices[ inputRoute.deviceId ]->sourceVolume();
    }

    return -1.0;
}


bool DFL::Pipewire::setSourceVolume( qreal volume ) {
    if ( mDevices.contains( inputRoute.deviceId ) ) {
        return mDevices[ inputRoute.deviceId ]->setSourceVolume( volume );
    }

    return false;
}


bool DFL::Pipewire::isSourceMuted() {
    if ( mDevices.contains( inputRoute.deviceId ) ) {
        return mDevices[ inputRoute.deviceId ]->isSourceMuted();
    }

    return false;
}


bool DFL::Pipewire::setSourceMuted( bool mute ) {
    if ( mDevices.contains( inputRoute.deviceId ) ) {
        return mDevices[ inputRoute.deviceId ]->setSourceMuted( mute );
    }

    return false;
}


QList<uint32_t> DFL::Pipewire::streams() {
    return mStreams.keys();
}


QMap<QString, QString> DFL::Pipewire::streamProperties( uint32_t ) {
    return QMap<QString, QString>();
}


bool DFL::Pipewire::setStreamVolume( uint32_t /*streamId*/, qreal /*vol*/ ) {
    return false;
}


void DFL::Pipewire::updateDefaultSink() {
    for ( uint32_t id: mSinks.keys() ) {
        if ( mSinks[ id ]->properties()[ "node.name" ] == outputRoute.name ) {
            outputRoute.nodeId = id;

            outputRoute.deviceId = mSinks[ id ]->properties()[ "device.id" ].toInt();
            outputRoute.ready    = false;

            // We're done getting the default device
            break;
        }
    }
}


void DFL::Pipewire::updateDefaultSource() {
    for ( uint32_t id: mSources.keys() ) {
        if ( mSources[ id ]->properties()[ "node.name" ] == inputRoute.name ) {
            inputRoute.nodeId = id;

            inputRoute.deviceId = mSources[ id ]->properties()[ "device.id" ].toInt();
            inputRoute.ready    = false;

            // We're done getting the default device
            break;
        }
    }
}


void DFL::Pipewire::run() {
    // Init pipewire
    DFL::WireAPI::initPW();

    // Create the Main Loop
    mMainLoop = new DFL::WireAPI::MainLoop();

    // Call setup: We don't have any connections for the events right now.
    mMainLoop->setup();

    // Create the Context
    mCtxt = new DFL::WireAPI::Context( *mMainLoop );

    // Call setup: We don't have any connections for the events right now.
    mCtxt->setup();

    // Create the Core.
    mCore = new DFL::WireAPI::Core( *mCtxt );

    // Loop to check if pipewire is running
    QEventLoop pwCheckLoop;

    // Timer to periodically check if the core becomes available
    QTimer pwCoreTimer;
    connect(
        &pwCoreTimer, &QTimer::timeout, this, [ this, &pwCheckLoop ]() {
            // Exit the event-loop @pwCheckLoop when mCore is valid
            if ( mCore->isValid() ) {
                pwCheckLoop.quit();
                return;
            }

            // Recreate the Core if it's not valid
            delete mCore;
            mCore = new DFL::WireAPI::Core( *mCtxt );
        }
    );

    // Check once a second
    pwCoreTimer.start( 1000 );
    pwCheckLoop.exec();

    // Stop the timer: to be stopped from this thread.
    pwCoreTimer.stop();

    // Try to gracefully quit when pipewire exits
    connect(
        mCore, &DFL::WireAPI::Core::error, [ this ] ( uint32_t, int, int res, QString ) {
            if ( res == -EPIPE ) {
                qCritical() << "Disconnected from server";

                /** Quit the main loop */
                mMainLoop->quit();

                /** Emit the disconnected signal. The user can restart the process */
                emit disconnected();
            }
        }
    );

    // Call setup: We're done setting up the signals now.
    mCore->setup();

    // We're connected to the server
    emit connected();

    // Let's create the registry
    mRegistry = new DFL::WireAPI::Registry( *mCore );

    // A device was added
    connect(
        mRegistry, &DFL::WireAPI::Registry::deviceAdded, [ = ] ( uint32_t id, uint32_t, const DFL::WireAPI::Properties& props ) {
            // Create the device object
            DFL::WireAPI::Device *device = new DFL::WireAPI::Device( mRegistry->bindDevice( id ), props, id );

            connect(
                device, &DFL::WireAPI::Device::defaultRouteChanged, this, [ this, device ] ( int direction ) {
                    /** direction == 1 means out. And that means sink */
                    if ( ( direction == 1 ) && ( this->outputRoute.deviceId == device->globalId() ) ) {
                        emit this->defaultSinkChanged( device->globalId() );
                    }

                    /** direction == o means in. And that means source */
                    if ( ( direction == 0 ) && ( this->inputRoute.deviceId == device->globalId() ) ) {
                        emit this->defaultSourceChanged( device->globalId() );
                    }
                }
            );

            connect(
                device, &DFL::WireAPI::Device::sinkPropertiesChanged, this, [ this, device ] ( DFL::WireAPI::Device::RouteProperties props ) {
                    if ( device->globalId() != this->outputRoute.deviceId ) {
                        return;
                    }

                    if ( outputRoute.properties.volume != props.volume ) {
                        emit this->sinkVolumeChanged( props.volume );
                    }

                    if ( outputRoute.properties.isMuted != props.isMuted ) {
                        emit this->sinkMuteChanged( props.isMuted );
                    }

                    this->outputRoute.properties = props;
                }
            );

            connect(
                device, &DFL::WireAPI::Device::sourcePropertiesChanged, this, [ this, device ] ( DFL::WireAPI::Device::RouteProperties props ) {
                    if ( device->globalId() != this->inputRoute.deviceId ) {
                        return;
                    }

                    if ( inputRoute.properties.volume != props.volume ) {
                        emit this->sourceVolumeChanged( props.volume );
                    }

                    if ( inputRoute.properties.isMuted != props.isMuted ) {
                        emit this->sourceMuteChanged( props.isMuted );
                    }

                    this->inputRoute.properties = props;
                }
            );

            // We want to get those route parameters
            device->setup();

            // Mutex locker to protect access to mDevices
            QMutexLocker locker( &mDeviceMutex );

            // Add the device object to the map
            mDevices[ id ] = device;
        }
    );

    // A node was added
    connect(
        mRegistry, &DFL::WireAPI::Registry::nodeAdded, [ = ] ( uint32_t id, uint32_t, const DFL::WireAPI::Properties& props ) {
            // Create the node object
            DFL::WireAPI::Node *node = new DFL::WireAPI::Node( mRegistry->bindNode( id ), props, id );


            // Update the default node
            // updateDefaultNode( node );

            if ( props[ "media.class" ].startsWith( "Stream/" ) ) {
                // Mutex locker to protect access to mStreams
                QMutexLocker locker( &mNodeMutex );

                // Add the node object to the map
                mStreams[ id ] = node;
            }

            else if ( props[ "media.class" ] == "Audio/Sink" ) {
                // Mutex locker to protect access to mStreams
                QMutexLocker locker( &mNodeMutex );

                // Add the node object to the map
                mSinks[ id ] = node;

                // Update the default Sink.
                updateDefaultSink();
            }

            else if ( props[ "media.class" ] == "Audio/Source" ) {
                // Mutex locker to protect access to mStreams
                QMutexLocker locker( &mNodeMutex );

                // Add the node object to the map
                mSources[ id ] = node;

                // Update the default Source.
                updateDefaultSource();
            }
        }
    );

    // A metadata was added
    connect(
        mRegistry, &DFL::WireAPI::Registry::metadataAdded, [ = ] ( uint32_t id, uint32_t, const DFL::WireAPI::Properties& props ) {
            // Create the device object
            DFL::WireAPI::Metadata *metadata = new DFL::WireAPI::Metadata( mRegistry->bindMetadata( id ), props, id );

            // The metadata that represents the defaults
            if ( props[ "metadata.name" ] == QString( "default" ) ) {
                connect(
                    metadata, &DFL::WireAPI::Metadata::propertyChanged, [ = ] ( uint32_t, QString key, QString, QString value ) {
                        if ( key == "default.audio.sink" ) {
                            QJsonDocument doc = QJsonDocument::fromJson( value.toUtf8() );
                            QJsonObject obj   = doc.object();

                            outputRoute.name = obj[ "name" ].toString();

                            updateDefaultSink();
                        }

                        else if ( key == "default.audio.source" ) {
                            QJsonDocument doc = QJsonDocument::fromJson( value.toUtf8() );
                            QJsonObject obj   = doc.object();

                            inputRoute.name = obj[ "name" ].toString();

                            updateDefaultSource();
                        }
                    }
                );
            }

            metadata->setup();

            // Update the default metadata
            // updateDefaultNode( metadata );

            // Mutex locker to protect access to mMetadatas
            QMutexLocker locker( &mMetadataMutex );

            // Add the metadata object to the map
            mMetadatas[ id ] = metadata;
        }
    );

    // An existing global was removed
    connect(
        mRegistry, &DFL::WireAPI::Registry::globalRemoved, [ = ] ( uint32_t id ) {
            if ( mDevices.contains( id ) ) {
                // Mutex locker to protect access to mDevices
                QMutexLocker locker( &mDeviceMutex );
                auto device = mDevices.take( id );

                device->disconnect();

                delete device;
            }

            else if ( mStreams.contains( id ) ) {
                // Mutex locker to protect access to mDevices
                QMutexLocker locker( &mNodeMutex );
                auto stream = mStreams.take( id );

                stream->disconnect();

                delete stream;
            }

            else if ( mSinks.contains( id ) ) {
                // Mutex locker to protect access to mDevices
                QMutexLocker locker( &mNodeMutex );
                auto sink = mSinks.take( id );

                sink->disconnect();

                delete sink;
            }

            else if ( mSources.contains( id ) ) {
                // Mutex locker to protect access to mDevices
                QMutexLocker locker( &mNodeMutex );
                auto source = mSources.take( id );

                source->disconnect();

                delete source;
            }

            else if ( mMetadatas.contains( id ) ) {
                // Mutex locker to protect access to mDevices
                QMutexLocker locker( &mMetadataMutex );
                auto metadata = mMetadatas.take( id );

                metadata->disconnect();

                delete metadata;
            }
        }
    );

    mRegistry->setup();

    // Run the main loop. This is a blocking function.
    mMainLoop->run();
}
