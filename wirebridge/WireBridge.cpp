/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtCore>

#include <iostream>
#include <cstring>

#include <pthread.h>
#include <pipewire/core.h>
#include <pipewire/pipewire.h>
#include <pipewire/extensions/metadata.h>

#include <spa/pod/pod.h>
#include <spa/pod/iter.h>
#include <spa/pod/builder.h>
#include <spa/param/props.h>

#include "WireBridge.hpp"

static inline DFL::PipeAPI::Properties convertSpaDictToMap( const struct spa_dict *dict ) {
    DFL::PipeAPI::Properties result;

    if ( !dict ) {
        return result;
    }

    const struct spa_dict_item *item;
    spa_dict_for_each( item, dict ) {
        result.insert( QString::fromUtf8( item->key ), QString::fromUtf8( item->value ) );
    }

    return result;
}


/**
 * DFL::PipeAPI::Monitor class
 *
 * This class initialises the connection to pipewire,
 * and allows us to create a pw_registry* object.
 * This class also hosts the pw_main_loop_run() function.
 * When the connection to pipewire is broken, this class
 * automatically tries to re-establish the connection.
 **/


DFL::PipeAPI::Monitor::Monitor( QObject *parent ) : QThread( parent ) {
}


DFL::PipeAPI::Monitor::~Monitor() {
    cleanup();

    /**
     * Do not call this when pipewire disconnects.
     * TODO: Ideally, create a proxy listener, so that we know
     * when it's destroyed.
     */
    // pw_proxy_destroy( (struct pw_proxy *)mCore );

    /**
     * Destroy the context.
     */
    if ( mCtxt ) {
        spa_hook_remove( &contextListener );
        pw_context_destroy( mCtxt );
    }

    if ( mLoop ) {
        pw_main_loop_destroy( mLoop );
    }

    pw_deinit();
}


void DFL::PipeAPI::Monitor::init( int argc, char *argv[] ) {
    pw_init( &argc, &argv );

    mLoop = pw_main_loop_new( nullptr );
    mCtxt = pw_context_new( pw_main_loop_get_loop( mLoop ), nullptr, 0 );

    if ( !mCtxt ) {
        qWarning() << "Failed to create Pipewire context.";
        return;
    }
}


void DFL::PipeAPI::Monitor::cleanup() {
    qDeleteAll( mSinks );
    mSinks.clear();

    qDeleteAll( mSources );
    mSources.clear();

    qDeleteAll( mStreams );
    mStreams.clear();

    qDeleteAll( mDevices );
    mDevices.clear();

    if ( mRegistry ) {
        qDebug() << "Deleting registry";
        spa_hook_remove( &registryListener );
        pw_proxy_destroy( (struct pw_proxy *)mRegistry );
    }

    if ( mCore ) {
        qDebug() << "Deleting core";
        spa_hook_remove( &coreListener );
        pw_core_disconnect( mCore );
    }

    /** Quit the main loop */
    pw_main_loop_quit( mLoop );
}


pw_main_loop * DFL::PipeAPI::Monitor::mainLoop() {
    return mLoop;
}


pw_context * DFL::PipeAPI::Monitor::context() {
    return mCtxt;
}


pw_core * DFL::PipeAPI::Monitor::core() {
    return mCore;
}


pw_registry * DFL::PipeAPI::Monitor::registry() {
    return mRegistry;
}


void DFL::PipeAPI::Monitor::run() {
    qDebug() << "Starting pw wait event loop";
    QEventLoop loop;
    QTimer     timer;

    // 500ms between attempts
    timer.setInterval( 500 );

    timer.callOnTimeout(
        [ & ]() {
            mCore = pw_context_connect( mCtxt, nullptr, 0 );

            if ( mCore ) {
                static const struct pw_core_events coreEvents = {
                    .version = PW_VERSION_CORE_EVENTS,
                    .error   = &DFL::PipeAPI::Monitor::coreEventError,
                };

                spa_zero( coreListener );
                pw_core_add_listener( mCore, &coreListener, &coreEvents, this );

                timer.stop();
                loop.quit();

                qInfo() << "Connected to Pipewire Server";

                mRegistry = pw_core_get_registry( mCore, PW_VERSION_REGISTRY, 0 );

                if ( !mRegistry ) {
                    qCritical() << "Failed to get Pipewire registry.";
                    return;
                }

                static const struct pw_registry_events registryEvents = {
                    PW_VERSION_REGISTRY_EVENTS,
                    registryEventGlobal,
                    registryEventGlobalRemove
                };

                spa_zero( registryListener );
                pw_registry_add_listener( mRegistry, &registryListener, &registryEvents, this );

                qInfo() << "Ready to recieve various events";
            }

            else {
                qDebug() << "Server unavailable";
            }
        }
    );

    qInfo() << "Waiting for Pipewire Server";

    timer.start();
    loop.exec();

    qDebug() << "Starting pipewire mainloop";
    pw_main_loop_run( mLoop );

    terminate();
}


void DFL::PipeAPI::Monitor::coreEventError( void *data, uint32_t id, int seq, int res, const char *message ) {
    auto monitor = static_cast<DFL::PipeAPI::Monitor *>( data );

    qDebug() << "Error on" << id << "in sequence" << seq << "with response" << res;
    qDebug() << "[ERROR]:" << message;

    if ( res == -EPIPE ) {
        // Connection to PipeWire lost
        qCritical() << "PipeWire Server disconnected:" << message;

        monitor->cleanup();

        emit monitor->serverDisconnected();
    }
}


DFL::PipeAPI::Node * DFL::PipeAPI::Monitor::defaultSink() {
    if ( mSinks.contains( defaultSinkId ) ) {
        return mSinks.value( defaultSinkId );
    }

    return nullptr;
}


DFL::PipeAPI::Node * DFL::PipeAPI::Monitor::defaultSource() {
    if ( mSources.contains( defaultSourceId ) ) {
        return mSources.value( defaultSourceId );
    }

    return nullptr;
}


void DFL::PipeAPI::Monitor::registryEventGlobal( void *data, uint32_t id, uint32_t, const char *type, uint32_t version, const struct spa_dict *props ) {
    auto *monitor = static_cast<DFL::PipeAPI::Monitor *>( data );

    if ( strcmp( type, PW_TYPE_INTERFACE_Device ) == 0 ) {
        Properties properties = convertSpaDictToMap( props );

        struct pw_device *device = static_cast<struct pw_device *>( pw_registry_bind( monitor->mRegistry, id, type, PW_VERSION_DEVICE, 0 ) );

        if ( device ) {
            qDebug() << "Device added" << id << properties[ "device.name" ] << properties[ "device.description" ];
            Device *deviceObj = new Device( device, properties, id );

            monitor->mDevices[ id ] = deviceObj;
            emit monitor->deviceAdded( deviceObj );
        }
    }

    else if ( strcmp( type, PW_TYPE_INTERFACE_Node ) == 0 ) {
        Properties properties = convertSpaDictToMap( props );

        if ( properties.value( PW_KEY_MEDIA_CLASS ) == "Audio/Sink" ) {
            struct pw_node *sink = static_cast<struct pw_node *>( pw_registry_bind( monitor->mRegistry, id, type, PW_VERSION_NODE, 0 ) );

            qDebug() << "Node" << id << "added" << properties[ "node.name" ] << properties[ "node.description" ] << properties[ "node.nick" ] << properties[ "object.path" ] << properties.keys();

            if ( sink ) {
                Node *sinkObj = new Node( sink, properties, id );
                monitor->mSinks[ id ] = sinkObj;

                emit monitor->sinkAdded( sinkObj );

                monitor->updateDefaultSinkId();
                emit monitor->defaultSinkChanged();
            }
        }

        else if ( properties.value( PW_KEY_MEDIA_CLASS ) == "Audio/Source" ) {
            struct pw_node *source = static_cast<struct pw_node *>( pw_registry_bind( monitor->mRegistry, id, type, PW_VERSION_NODE, 0 ) );

            if ( source ) {
                Node *sourceObj = new Node( source, properties, id );
                monitor->mSources[ id ] = sourceObj;

                emit monitor->sourceAdded( sourceObj );

                monitor->updateDefaultSourceId();
                emit monitor->defaultSourceChanged();
            }
        }

        else if ( properties.value( PW_KEY_MEDIA_CLASS ).startsWith( "Stream" ) ) {
            struct pw_node *stream = static_cast<struct pw_node *>( pw_registry_bind( monitor->mRegistry, id, type, PW_VERSION_NODE, 0 ) );

            if ( stream ) {
                Node *streamObj = new Node( stream, properties, id );
                monitor->mStreams[ id ] = streamObj;

                qDebug() << "New stream started" << id;

                emit monitor->streamAdded( streamObj );
            }
        }
    }

    else if ( strcmp( type, PW_TYPE_INTERFACE_Port ) == 0 ) {
        Properties properties = convertSpaDictToMap( props );

        int node_id = properties[ "node.id" ].toInt();
        // int device_id = properties[ "device.id" ].toInt();
        // int port_id   = properties[ "port.id" ].toInt();

        if ( monitor->mSinks.contains( node_id ) ) {
            struct pw_port *port = static_cast<struct pw_port *>( pw_registry_bind( monitor->mRegistry, id, type, PW_VERSION_PORT, 0 ) );

            if ( port ) {
                monitor->mSinks[ node_id ]->addPort( new Port( port, properties, id ) );
                monitor->mPortNodeMap[ id ] = node_id;
            }

            qDebug() << "Port" << id << "added to Sink " << node_id << "on Device" << monitor->mSinks[ node_id ]->properties()[ "device.id" ].toUtf8().data();
        }

        else if ( monitor->mSources.contains( node_id ) ) {
            struct pw_port *port = static_cast<struct pw_port *>( pw_registry_bind( monitor->mRegistry, id, type, PW_VERSION_PORT, 0 ) );

            if ( port ) {
                monitor->mSources[ node_id ]->addPort( new Port( port, properties, id ) );
                monitor->mPortNodeMap[ id ] = node_id;
            }

            qDebug() << "Port" << id << "added to Source " << node_id << "on Device" << monitor->mSources[ node_id ]->properties()[ "device.id" ].toUtf8().data();
        }

        else {
        }
    }
}


void DFL::PipeAPI::Monitor::registryEventGlobalRemove( void *data, uint32_t id ) {
    auto *monitor = static_cast<DFL::PipeAPI::Monitor *>( data );

    if ( monitor->mDevices.contains( id ) ) {
        Device *device = monitor->mDevices.take( id );
        delete device;
    }

    else if ( monitor->mSinks.contains( id ) ) {
        Node *sink = monitor->mSinks.take( id );
        delete sink;
    }

    else if ( monitor->mSources.contains( id ) ) {
        Node *source = monitor->mSources.take( id );
        delete source;
    }

    else if ( monitor->mStreams.contains( id ) ) {
        Node *stream = monitor->mStreams.take( id );
        delete stream;
    }

    else if ( monitor->mPortNodeMap.contains( id ) ) {
        uint32_t nodeId = monitor->mPortNodeMap[ id ];

        if ( monitor->mSinks.contains( nodeId ) ) {
            monitor->mSinks[ nodeId ]->removePort( id );
        }

        if ( monitor->mSources.contains( nodeId ) ) {
            monitor->mSources[ nodeId ]->removePort( id );
        }
    }
}


void DFL::PipeAPI::Monitor::updateDefaultSinkId() {
    if ( mSinks.count() == 0 ) {
        return;
    }

    else {
        Node *best = nullptr;
        for ( Node *source: mSinks) {
            if ( best == nullptr ) {
                best = source;
            }

            else {
                if ( best->properties()[ "priority.session" ].toInt() < source->properties()[ "priority.session" ].toInt() ) {
                    best = source;
                }
            }
        }

        defaultSinkId = best->globalId();
    }
}


void DFL::PipeAPI::Monitor::updateDefaultSourceId() {
    if ( mSources.count() == 0 ) {
        return;
    }

    else {
        Node *best = nullptr;
        for ( Node *source: mSources) {
            if ( best == nullptr ) {
                best = source;
            }

            else {
                if ( best->properties()[ "priority.session" ].toInt() < source->properties()[ "priority.session" ].toInt() ) {
                    best = source;
                }
            }
        }

        defaultSourceId = best->globalId();
    }
}


/**
** DFL::PipeAPI::Device class
**
** This class wraps the pw_device object.
**/


DFL::PipeAPI::Device::Device( struct pw_device *device, Properties props, uint32_t id ) : QObject() {
    mDevice   = device;
    mGlobalId = id;

    mProperties = props;

    spa_zero( deviceListener );

    static const struct pw_device_events deviceEvents = {
        .version = PW_VERSION_DEVICE_EVENTS,
        .info    = &DFL::PipeAPI::Device::deviceEventInfo,
        .param   = &DFL::PipeAPI::Device::deviceEventParam,
    };

    pw_device_add_listener( mDevice, &deviceListener, &deviceEvents, this );

    // Define the parameter IDs to subscribe to
    uint32_t ids[] = { SPA_PARAM_Props, SPA_PARAM_Route };

    // Subscribe to these parameters
    pw_device_subscribe_params( mDevice, ids, 2 );
}


DFL::PipeAPI::Device::~Device() {
    spa_hook_remove( &deviceListener );
    pw_proxy_destroy( (struct pw_proxy *)mDevice );
}


struct pw_device *DFL::PipeAPI::Device::get() {
    return mDevice;
}


uint32_t DFL::PipeAPI::Device::globalId() {
    return mGlobalId;
}


DFL::PipeAPI::Properties DFL::PipeAPI::Device::properties() {
    return mProperties;
}


qreal DFL::PipeAPI::Device::volume() {
    return 1.0;
}


bool DFL::PipeAPI::Device::setVolume( qreal ) {
    return 1.0;
}


void DFL::PipeAPI::Device::deviceEventInfo( void *data, const struct pw_device_info *info ) {
    auto *device = static_cast<DFL::PipeAPI::Device *>( data );

    if ( info->change_mask & PW_DEVICE_CHANGE_MASK_PROPS ) {
        emit device->propertiesChanged();
        device->mProperties = convertSpaDictToMap( info->props );
    }

    if ( info->change_mask & PW_DEVICE_CHANGE_MASK_PARAMS ) {
        emit device->parametersChanged();
    }
}


void DFL::PipeAPI::Device::deviceEventParam( void *data, int, uint32_t id, uint32_t, uint32_t, const struct spa_pod *param ) {
    if ( !param ) {
        qWarning() << "deviceEventParam: Received a null parameter.";
        return;
    }

    auto *device = static_cast<DFL::PipeAPI::Device *>( data );

    if ( id == SPA_PARAM_Props ) {
        if ( spa_pod_is_object( param ) ) {
            struct spa_pod_object *obj = (struct spa_pod_object *)param;

            // Iterate through the properties in the object
            struct spa_pod_prop *iter;
            for ( iter = spa_pod_prop_first( &( obj->body ) ); spa_pod_prop_is_inside( &( obj->body ), param->size, iter ); iter = spa_pod_prop_next( iter ) ) {
                if ( iter->key == SPA_PROP_volume ) {
                    float volume = 0;
                    spa_pod_get_float( &( iter->value ), &volume );
                    device->mProperties[ "volume" ] = QString::number( volume );
                    qDebug() << "Device" << device->globalId() << volume;
                }

                else if ( iter->key == SPA_PROP_softMute ) {
                    bool mute = false;
                    spa_pod_get_bool( &( iter->value ), &mute );
                    device->mProperties[ "mute" ] = ( mute ? "true" : "false" );
                    qDebug() << "Device" << device->globalId() << mute;
                }

                else {
                }
            }
        }
    }

    else if ( id == SPA_PARAM_Route ) {
        if ( spa_pod_is_object( param ) ) {
            struct spa_pod_object *obj = (struct spa_pod_object *)param;

            qDebug() << "Device" << device->mGlobalId;

            // Iterate through the properties in the object
            struct spa_pod_prop *iter;
            for ( iter = spa_pod_prop_first( &( obj->body ) ); spa_pod_prop_is_inside( &( obj->body ), param->size, iter ); iter = spa_pod_prop_next( iter ) ) {
                switch ( iter->key ) {
                    case SPA_PARAM_ROUTE_index: {
                        int value = 0;
                        spa_pod_get_int( &( iter->value ), &value );
                        qDebug() << "    index         :" << value;

                        break;
                    }

                    case SPA_PARAM_ROUTE_direction: {
                        uint32_t value = 0;
                        spa_pod_get_id( &( iter->value ), &value );
                        qDebug() << "    direction     :" << value;

                        break;
                    }

                    case SPA_PARAM_ROUTE_device: {
                        int value = 0;
                        spa_pod_get_int( &( iter->value ), &value );
                        qDebug() << "    device        :" << value;

                        break;
                    }

                    case SPA_PARAM_ROUTE_name: {
                        const char *value = { 0 };
                        spa_pod_get_string( &( iter->value ), &value );
                        qDebug() << "    name          :" << value;

                        break;
                    }

                    case SPA_PARAM_ROUTE_description: {
                        const char *value = { 0 };
                        spa_pod_get_string( &( iter->value ), &value );
                        qDebug() << "    description   :" << value;

                        break;
                    }

                    case SPA_PARAM_ROUTE_priority: {
                        int value = 0;
                        spa_pod_get_int( &( iter->value ), &value );
                        qDebug() << "    priority      :" << value;

                        break;
                    }

                    case SPA_PARAM_ROUTE_available: {
                        uint32_t value = 0;
                        spa_pod_get_id( &( iter->value ), &value );
                        qDebug() << "    available     :" << value;

                        break;
                    }

                    case SPA_PARAM_ROUTE_info: {
                        // int value = 0;
                        // spa_pod_get_int( &(iter->value), &value );
                        // qDebug() << "    info          :" << value;

                        break;
                    }

                    case SPA_PARAM_ROUTE_profiles: {
                        int value = 0;
                        spa_pod_get_int( &( iter->value ), &value );
                        qDebug() << "    profiles      :" << value;

                        break;
                    }

                    case SPA_PARAM_ROUTE_props: {
                        // int value = 0;
                        // spa_pod_get_int( &(iter->value), &value );
                        // qDebug() << "    props         :" << value;

                        break;
                    }

                    case SPA_PARAM_ROUTE_devices: {
                        int value = 0;
                        spa_pod_get_int( &( iter->value ), &value );
                        qDebug() << "    devices       :" << value;

                        break;
                    }

                    case SPA_PARAM_ROUTE_profile: {
                        int value = 0;
                        spa_pod_get_int( &( iter->value ), &value );
                        qDebug() << "    profile       :" << value;

                        break;
                    }

                    case SPA_PARAM_ROUTE_save: {
                        bool value = 0;
                        spa_pod_get_bool( &( iter->value ), &value );
                        qDebug() << "    save          :" << value;

                        break;
                    }

                    default: {
                        qDebug() << "    Unknown key" << iter->key;
                        break;
                    }
                }
            }
            qDebug() << "";
        }
    }
}


/**
** DFL::PipeAPI::Node class
**
** This class wraps the pw_node object.
** The notifications when new ports are added or removed
** are emitted from here.
**/


DFL::PipeAPI::Node::Node( struct pw_node *node, Properties props, uint32_t id ) : QObject() {
    mNode     = node;
    mGlobalId = id;

    mProperties = props;

    // Define the parameter IDs to subscribe to
    uint32_t ids[] = { SPA_PARAM_Props };

    // Subscribe to these parameters
    pw_node_subscribe_params( mNode, ids, 1 );

    spa_zero( nodeListener );

    static const struct pw_node_events nodeEvents = {
        .version = PW_VERSION_NODE_EVENTS,
        .info    = &DFL::PipeAPI::Node::nodeEventInfo,
        .param   = &DFL::PipeAPI::Node::nodeEventParam,
    };

    pw_node_add_listener( mNode, &nodeListener, &nodeEvents, this );
}


DFL::PipeAPI::Node::~Node() {
    qDebug() << "Deleting node" << mGlobalId;
    spa_hook_remove( &nodeListener );
    pw_proxy_destroy( (struct pw_proxy *)mNode );
}


struct pw_node *DFL::PipeAPI::Node::get() {
    return mNode;
}


uint32_t DFL::PipeAPI::Node::globalId() {
    return mGlobalId;
}


DFL::PipeAPI::Properties DFL::PipeAPI::Node::properties() {
    return mProperties;
}


DFL::PipeAPI::Node::State DFL::PipeAPI::Node::state() {
    return mState;
}


QString DFL::PipeAPI::Node::error() {
    return mError;
}


qreal DFL::PipeAPI::Node::volume() {
    return mProperties[ "volume" ].toDouble();
}


bool DFL::PipeAPI::Node::setVolume( qreal volume ) {
    uint8_t                buffer[ 1024 ];
    struct spa_pod_builder builder;
    struct spa_pod_frame   frame;

    // Initialize the builder with the buffer
    spa_pod_builder_init( &builder, buffer, sizeof( buffer ) );

    // Begin constructing an object pod for SPA_TYPE_OBJECT_Props
    spa_pod_builder_push_object(
        &builder,
        &frame,
        SPA_TYPE_OBJECT_Props,  // Object type
        SPA_PARAM_Props         // Parameter ID
    );

    // Add the volume property to the object
    spa_pod_builder_add(
        &builder,
        SPA_PROP_volume, SPA_POD_Float( (float)volume ), // Property: volume
        0
    );

    // Finalize the object
    struct spa_pod *pod = (struct spa_pod *)spa_pod_builder_pop( &builder, &frame );

    // Use the constructed pod to set the parameter
    int result = pw_node_set_param(
        mNode,
        SPA_PARAM_Props, // Parameter type
        0,               // Flags (none)
        pod              // Pod containing the volume property
    );

    return result >= 0;
}


bool DFL::PipeAPI::Node::isMuted() {
    return ( mProperties[ "mute" ] == "true" ? true : false );
}


bool DFL::PipeAPI::Node::setMuted( bool mute ) {
    uint8_t                buffer[ 1024 ];
    struct spa_pod_builder builder;

    spa_pod_builder_init( &builder, buffer, sizeof( buffer ) );

    struct spa_pod_frame frame;
    spa_pod_builder_push_object(
        &builder,
        &frame,
        SPA_TYPE_OBJECT_Props,
        0
    );

    spa_pod_builder_add(
        &builder,
        SPA_PROP_mute, SPA_POD_Bool( mute ),
        0
    );

    struct spa_pod *pod = (struct spa_pod *)spa_pod_builder_pop( &builder, &frame );

    int result = pw_node_set_param(
        mNode,
        SPA_PARAM_Props,
        0,
        pod
    );

    return result >= 0;
}


void DFL::PipeAPI::Node::addPort( Port *port ) {
    mPorts[ port->globalId() ] = port;
    emit portAdded( port );
}


void DFL::PipeAPI::Node::removePort( uint32_t id ) {
    Port *p = mPorts.take( id );

    delete p;

    emit portRemoved( id );
}


bool DFL::PipeAPI::Node::hasPort( uint32_t id ) {
    return mPorts.contains( id );
}


void DFL::PipeAPI::Node::nodeEventInfo( void *data, const struct pw_node_info *info ) {
    auto *node = static_cast<DFL::PipeAPI::Node *>( data );

    if ( info->change_mask & PW_NODE_CHANGE_MASK_STATE ) {
        emit node->stateChanged();
        node->mState = (State)info->state;
        node->mError = ( info->state == -1 ? info->error : "" );
    }

    if ( info->change_mask & PW_NODE_CHANGE_MASK_PROPS ) {
        emit node->propertiesChanged();
        node->mProperties = convertSpaDictToMap( info->props );
    }

    if ( info->change_mask & PW_NODE_CHANGE_MASK_PARAMS ) {
        emit node->parametersChanged();
    }
}


void DFL::PipeAPI::Node::nodeEventParam( void *data, int, uint32_t id, uint32_t, uint32_t, const struct spa_pod *param ) {
    if ( !param ) {
        qWarning() << "nodeEventParam: Received a null parameter.";
        return;
    }

    auto *node = static_cast<DFL::PipeAPI::Node *>( data );

    if ( id == SPA_PARAM_Props ) {
        if ( spa_pod_is_object( param ) ) {
            struct spa_pod_object *obj = (struct spa_pod_object *)param;

            // Iterate through the properties in the object
            struct spa_pod_prop *iter;
            for ( iter = spa_pod_prop_first( &( obj->body ) ); spa_pod_prop_is_inside( &( obj->body ), param->size, iter ); iter = spa_pod_prop_next( iter ) ) {
                if ( iter->key == SPA_PROP_volume ) {
                    float volume = 0;
                    spa_pod_get_float( &( iter->value ), &volume );
                    node->mProperties[ "volume" ] = QString::number( volume );
                }

                else if ( iter->key == SPA_PROP_softMute ) {
                    bool mute = false;
                    spa_pod_get_bool( &( iter->value ), &mute );
                    node->mProperties[ "mute" ] = ( mute ? "true" : "false" );
                }

                else {
                }
            }
        }
    }
}


/**
** DFL::PipeAPI::Port class
**
** This class wraps the pw_port object.
**/


DFL::PipeAPI::Port::Port( struct pw_port *port, Properties props, uint32_t id ) : QObject() {
    mPort     = port;
    mGlobalId = id;

    mProperties = props;

    spa_zero( portListener );

    static const struct pw_port_events portEvents = {
        .version = PW_VERSION_PORT_EVENTS,
        .info    = &DFL::PipeAPI::Port::portEventInfo,
        .param   = &DFL::PipeAPI::Port::portEventParam,
    };

    pw_port_add_listener( mPort, &portListener, &portEvents, this );

    // Define the parameter IDs to subscribe to
    uint32_t ids[] = { SPA_PARAM_Props };

    // Subscribe to these parameters
    pw_port_subscribe_params( mPort, ids, 1 );
}


DFL::PipeAPI::Port::~Port() {
    spa_hook_remove( &portListener );
    pw_proxy_destroy( (struct pw_proxy *)mPort );
}


struct pw_port *DFL::PipeAPI::Port::get() {
    return mPort;
}


uint32_t DFL::PipeAPI::Port::globalId() {
    return mGlobalId;
}


DFL::PipeAPI::Properties DFL::PipeAPI::Port::properties() {
    return mProperties;
}


qreal DFL::PipeAPI::Port::volume() {
    return mProperties[ "volume" ].toDouble();
}


bool DFL::PipeAPI::Port::isMuted() {
    return ( mProperties[ "mute" ] == "true" ? true : false );
}


void DFL::PipeAPI::Port::portEventInfo( void *data, const struct pw_port_info *info ) {
    auto *port = static_cast<DFL::PipeAPI::Port *>( data );

    if ( info->change_mask & PW_PORT_CHANGE_MASK_PROPS ) {
        emit port->propertiesChanged();
        port->mProperties = convertSpaDictToMap( info->props );
    }

    if ( info->change_mask & PW_PORT_CHANGE_MASK_PARAMS ) {
        emit port->parametersChanged();
    }
}


void DFL::PipeAPI::Port::portEventParam( void *data, int, uint32_t id, uint32_t, uint32_t, const struct spa_pod *param ) {
    if ( !param ) {
        qWarning() << "portEventParam: Received a null parameter.";
        return;
    }

    auto *port = static_cast<DFL::PipeAPI::Port *>( data );

    if ( id == SPA_PARAM_Props ) {
        if ( spa_pod_is_object( param ) ) {
            struct spa_pod_object *obj = (struct spa_pod_object *)param;

            // Iterate through the properties in the object
            struct spa_pod_prop *iter;
            for ( iter = spa_pod_prop_first( &( obj->body ) ); spa_pod_prop_is_inside( &( obj->body ), param->size, iter ); iter = spa_pod_prop_next( iter ) ) {
                if ( iter->key == SPA_PROP_volume ) {
                    float volume = 0;
                    spa_pod_get_float( &( iter->value ), &volume );
                    port->mProperties[ "volume" ] = QString::number( volume );
                }

                else if ( iter->key == SPA_PROP_mute ) {
                    bool mute = false;
                    spa_pod_get_bool( &( iter->value ), &mute );
                    port->mProperties[ "mute" ] = ( mute ? "true" : "false" );
                }
            }
        }
    }
}
