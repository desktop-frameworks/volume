/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDebug>
#include <QVariant>
#include <QVariantList>
#include <iostream>

#include "WireAPI/Metadata.hpp"

#include <pipewire/pipewire.h>
#include <pipewire/extensions/metadata.h>
#include <spa/pod/pod.h>
#include <spa/pod/iter.h>
#include <spa/param/props.h>


DFL::WireAPI::Metadata::Metadata( struct pw_metadata *metadata, Properties props, uint32_t id, QObject *parent ) : DFL::WireAPI::Proxy( reinterpret_cast<pw_proxy *>( metadata ), parent ) {
    mObj      = metadata;
    mGlobalId = id;

    mProperties = props;

    mListener = new struct spa_hook ( );
    mEvents   = new struct pw_metadata_events ( );

    *mEvents      = {
        .version  = PW_VERSION_METADATA_EVENTS,
        .property = &DFL::WireAPI::Metadata::handleProperty,
    };

    pw_metadata_add_listener( mObj, mListener, mEvents, this );
}


DFL::WireAPI::Metadata::~Metadata() {
    spa_hook_remove( mListener );

    delete mListener;
    delete mEvents;
}


void DFL::WireAPI::Metadata::setup() {
    if ( mIsSetup == false ) {
        // Call setup() of parent class Proxy
        // Events of Proxy will be emitted here.
        Proxy::setup();

        while ( mPendingProps.count() ) {
            QString      key    = mPendingProps.keys().first();
            QVariantList values = mPendingProps.take( key );

            emit propertyChanged( values.at( 0 ).toInt(), key, values.at( 1 ).toString(), values.at( 2 ).toString() );
        }
    }
}


struct pw_metadata *DFL::WireAPI::Metadata::pwMetadata() {
    return mObj;
}


struct pw_metadata *DFL::WireAPI::Metadata::pwMetadata() const {
    return mObj;
}


QString DFL::WireAPI::Metadata::name() const {
    return mProperties[ "metadata.name" ];
}


uint32_t DFL::WireAPI::Metadata::globalId() {
    return mGlobalId;
}


DFL::WireAPI::Properties DFL::WireAPI::Metadata::properties() {
    return mProperties;
}


int DFL::WireAPI::Metadata::handleProperty( void *data, uint32_t subject, const char *key, const char *type, const char *value ) {
    auto *metadata = static_cast<DFL::WireAPI::Metadata *>( data );

    if ( metadata->mIsSetup ) {
        emit metadata->propertyChanged( subject, key, type, value );
        metadata->mAcquiredProps[ key ] = QVariantList() << QVariant( subject ) << QVariant( type ) << QVariant( value );
    }

    else {
        metadata->mPendingProps[ key ] = QVariantList() << subject << type << value;
    }

    return 0;
}
