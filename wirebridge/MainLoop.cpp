/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDebug>
#include "WireAPI/MainLoop.hpp"
#include <pipewire/pipewire.h>

struct spa_hook            DFL::WireAPI::MainLoop::mListener = {};
struct pw_main_loop_events DFL::WireAPI::MainLoop::mEvents   = {
    .version = PW_VERSION_MAIN_LOOP_EVENTS,
    .destroy = &DFL::WireAPI::MainLoop::handleDestroy,
};

DFL::WireAPI::MainLoop::MainLoop( QObject *parent ) : DFL::WireAPI::Base( parent ), mObj( pw_main_loop_new( nullptr ), MainloopDeleter( this ) ) {
    spa_zero( mListener );
    pw_main_loop_add_listener( mObj.get(), &mListener, &mEvents, this );
}


DFL::WireAPI::MainLoop::~MainLoop() {
    spa_hook_remove( &mListener );
}


void DFL::WireAPI::MainLoop::setup() {
    if ( mIsSetup == false ) {
        mIsSetup = true;

        if ( mPending ) {
            emit destroyed();
        }
    }
}


bool DFL::WireAPI::MainLoop::isValid() const {
    return mIsValid;
}


int DFL::WireAPI::MainLoop::run() {
    return pw_main_loop_run( mObj.get() );
}


int DFL::WireAPI::MainLoop::quit() {
    return pw_main_loop_quit( mObj.get() );
}


struct pw_loop * DFL::WireAPI::MainLoop::getPwLoop() const {
    return pw_main_loop_get_loop( mObj.get() );
}


struct pw_main_loop * DFL::WireAPI::MainLoop::pwMainLoop() {
    return mObj.get();
}


struct pw_main_loop * DFL::WireAPI::MainLoop::pwMainLoop() const {
    return mObj.get();
}


void DFL::WireAPI::MainLoop::MainLoop::handleDestroy( void *data ) {
    DFL::WireAPI::MainLoop *mLoop = reinterpret_cast<DFL::WireAPI::MainLoop *>( data );

    if ( mLoop ) {
        if ( mLoop->mIsSetup ) {
            mLoop->mObj.reset( nullptr );
        }

        else {
            mLoop->mPending = true;
        }
    }
}


/**
 * Deleter Struct
 */

DFL::WireAPI::MainLoop::MainloopDeleter::MainloopDeleter( DFL::WireAPI::MainLoop *instance ) : mainLoopInstance( instance ) {
    // Nothing else to do
}


void DFL::WireAPI::MainLoop::MainloopDeleter::operator()( struct pw_main_loop *pointer ) const {
    // Call the destructor only if the pointer is valid.
    if ( mainLoopInstance->isValid() ) {
        // Perform the destruction
        pw_main_loop_destroy( pointer );

        // Set the pointer to nullptr
        pointer = nullptr;
    }
}
