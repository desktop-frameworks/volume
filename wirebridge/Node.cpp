/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDebug>
#include <iostream>

#include "WireAPI/Node.hpp"

#include <pipewire/pipewire.h>
#include <spa/pod/pod.h>
#include <spa/pod/iter.h>
#include <spa/pod/builder.h>
#include <spa/param/props.h>


DFL::WireAPI::Node::Node( struct pw_node *node, Properties props, uint32_t id, QObject *parent ) : DFL::WireAPI::Proxy( reinterpret_cast<pw_proxy *>( node ), parent ) {
    mObj      = node;
    mGlobalId = id;

    mProperties = props;

    mListener = new struct spa_hook ( );
    mEvents   = new struct pw_node_events ( );

    *mEvents     = {
        .version = PW_VERSION_NODE_EVENTS,
        .info    = &DFL::WireAPI::Node::nodeEventInfo,
        .param   = &DFL::WireAPI::Node::nodeEventParam,
    };

    pw_node_add_listener( mObj, mListener, mEvents, this );

    // Define the parameter IDs to subscribe to
    uint32_t ids[] = { SPA_PARAM_Props };

    // Subscribe to these parameters
    pw_node_subscribe_params( mObj, ids, 1 );
}


DFL::WireAPI::Node::~Node() {
    spa_hook_remove( mListener );
    delete mListener;
    delete mEvents;
}


void DFL::WireAPI::Node::setup() {
    if ( mIsSetup == false ) {
        // Call setup() of parent class Proxy
        // Events of Proxy will be emitted here.
        Proxy::setup();

        if ( mPendingState ) {
            emit stateChanged();
        }

        if ( mPendingProperties ) {
            emit propertiesChanged();
        }

        if ( mPendingParameters ) {
            emit parametersChanged();
        }
    }
}


struct pw_node *DFL::WireAPI::Node::pwNode() {
    return mObj;
}


struct pw_node *DFL::WireAPI::Node::pwNode() const {
    return mObj;
}


uint32_t DFL::WireAPI::Node::globalId() {
    return mGlobalId;
}


DFL::WireAPI::Properties DFL::WireAPI::Node::properties() {
    return mProperties;
}


DFL::WireAPI::Node::State DFL::WireAPI::Node::state() {
    return mState;
}


QString DFL::WireAPI::Node::error() {
    return mError;
}


qreal DFL::WireAPI::Node::volume() {
    return mProperties[ "volume" ].toDouble();
}


bool DFL::WireAPI::Node::setVolume( qreal volume ) {
    uint8_t                buffer[ 1024 ];
    struct spa_pod_builder builder;
    struct spa_pod_frame   frame;

    // Initialize the builder with the buffer
    spa_pod_builder_init( &builder, buffer, sizeof( buffer ) );

    // Begin constructing an object pod for SPA_TYPE_OBJECT_Props
    spa_pod_builder_push_object(
        &builder,
        &frame,
        SPA_TYPE_OBJECT_Props,  // Object type
        SPA_PARAM_Props         // Parameter ID
    );

    // Add the volume property to the object
    spa_pod_builder_add(
        &builder,
        SPA_PROP_volume, SPA_POD_Float( (float)volume ), // Property: volume
        0
    );

    // Finalize the object
    struct spa_pod *pod = (struct spa_pod *)spa_pod_builder_pop( &builder, &frame );

    // Use the constructed pod to set the parameter
    int result = pw_node_set_param(
        mObj,
        SPA_PARAM_Props, // Parameter type
        0,               // Flags (none)
        pod              // Pod containing the volume property
    );

    return result >= 0;
}


bool DFL::WireAPI::Node::isMuted() {
    return ( mProperties[ "mute" ] == "true" ? true : false );
}


bool DFL::WireAPI::Node::setMuted( bool mute ) {
    uint8_t                buffer[ 1024 ];
    struct spa_pod_builder builder;

    spa_pod_builder_init( &builder, buffer, sizeof( buffer ) );

    struct spa_pod_frame frame;
    spa_pod_builder_push_object(
        &builder,
        &frame,
        SPA_TYPE_OBJECT_Props,
        0
    );

    spa_pod_builder_add(
        &builder,
        SPA_PROP_mute, SPA_POD_Bool( mute ),
        0
    );

    struct spa_pod *pod = (struct spa_pod *)spa_pod_builder_pop( &builder, &frame );

    int result = pw_node_set_param(
        mObj,
        SPA_PARAM_Props,
        0,
        pod
    );

    return result >= 0;
}


// void DFL::WireAPI::Node::addPort( Port *port ) {
//     mPorts[ port->globalId() ] = port;
//     emit portAdded( port );
// }


// void DFL::WireAPI::Node::removePort( uint32_t id ) {
//     Port *p = mPorts.take( id );

//     delete p;

//     emit portRemoved( id );
// }


// bool DFL::WireAPI::Node::hasPort( uint32_t id ) {
//     return mPorts.contains( id );
// }


void DFL::WireAPI::Node::nodeEventInfo( void *data, const struct pw_node_info *info ) {
    // Check for null pointers
    if ( !data || !info ) {
        qWarning() << "nodeEventInfo: data or info is null";
        return;
    }

    auto *node = static_cast<DFL::WireAPI::Node *>( data );

    // Ensure thread-safe access to device data
    QMutexLocker locker( &node->mMutex );

    if ( info->change_mask & PW_NODE_CHANGE_MASK_STATE ) {
        node->mState = (State)info->state;
        node->mError = ( info->state == -1 ? info->error : "" );

        if ( node->mIsSetup ) {
            emit node->stateChanged();
        }

        else {
            node->mPendingState = true;
        }
    }

    if ( info->change_mask & PW_NODE_CHANGE_MASK_PROPS ) {
        node->mProperties = convertSpaDictToMap( info->props );

        if ( node->mIsSetup ) {
            emit node->propertiesChanged();
        }

        else {
            node->mPendingProperties = true;
        }
    }

    if ( info->change_mask & PW_NODE_CHANGE_MASK_PARAMS ) {
        if ( node->mIsSetup ) {
            emit node->parametersChanged();
        }

        else {
            node->mPendingParameters = true;
        }
    }
}


void DFL::WireAPI::Node::nodeEventParam( void *data, int, uint32_t id, uint32_t, uint32_t, const struct spa_pod *param ) {
    if ( !param ) {
        qWarning() << "nodeEventParam: Received a null parameter.";
        return;
    }

    auto *node = static_cast<DFL::WireAPI::Node *>( data );

    if ( id == SPA_PARAM_Props ) {
        if ( spa_pod_is_object( param ) ) {
            struct spa_pod_object *obj = (struct spa_pod_object *)param;

            // Iterate through the properties in the object
            struct spa_pod_prop *iter;
            for ( iter = spa_pod_prop_first( &( obj->body ) ); spa_pod_prop_is_inside( &( obj->body ), param->size, iter ); iter = spa_pod_prop_next( iter ) ) {
                if ( iter->key == SPA_PROP_volume ) {
                    float volume = 0;
                    spa_pod_get_float( &( iter->value ), &volume );
                    node->mProperties[ "volume" ] = QString::number( volume );
                }

                else if ( iter->key == SPA_PROP_softMute ) {
                    bool mute = false;
                    spa_pod_get_bool( &( iter->value ), &mute );
                    node->mProperties[ "mute" ] = ( mute ? "true" : "false" );
                }

                else {
                }
            }
        }
    }
}
