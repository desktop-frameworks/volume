/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>

namespace DFL {
    namespace WireAPI {
        class Base;
    }
}

/**
 * This is the base class from which all other WireAPI class will be derived.
 */
class DFL::WireAPI::Base : public QObject {
    Q_OBJECT;

    public:
        // Default contructor
        Base( QObject *parent = nullptr );

        // Default destructor
        ~Base();

        // Funtion to inform the class that it can start relaying the events.
        // This implementation just toggles the state of mIsSetup flag.
        virtual void setup();

        // A check to know if this object is valid
        virtual bool isValid() const;

    protected:
        // Flag to check if this object is valid
        mutable bool mIsValid = false;

        // Flag to check if this object is setup
        mutable bool mIsSetup = false;
};
