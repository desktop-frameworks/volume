/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>
#include <QMutex>
#include <QThread>

#include "WireAPI/Pipewire.hpp"
#include "WireAPI/Proxy.hpp"

struct pw_device;
struct pw_proxy;
struct spa_hook;
struct spa_pod;
struct pw_device_events;
struct pw_device_info;

namespace DFL {
    namespace WireAPI {
        class Registry;
        class Device;
    }
}

class DFL::WireAPI::Device : public DFL::WireAPI::Proxy {
    Q_OBJECT;

    public:
        Device( struct pw_device *, Properties props, uint32_t id, QObject *parent = nullptr );
        ~Device();

        // Get the underlying raw pointer
        struct pw_device *pwDevice();

        // Get the underlying raw pointer
        struct pw_device *pwDevice() const;

        // Override the setup() of Proxy
        void setup() override;

        // Get the global id of this object
        uint32_t globalId();

        // Get the device properties
        Properties properties();

        /** Volume getter/setter */
        qreal sinkVolume();
        bool setSinkVolume( qreal );

        /** Mic volume getter/setter */
        qreal sourceVolume();
        bool setSourceVolume( qreal );

        /** Mute getter/setter */
        bool isSinkMuted();
        bool setSinkMuted( bool );

        /** Mic mute getter/setter */
        bool isSourceMuted();
        bool setSourceMuted( bool );

        Q_SIGNAL void parametersChanged();
        Q_SIGNAL void propertiesChanged();

        Q_SIGNAL void defaultRouteChanged( int );

        typedef struct route_properties_t {
            // Route name and description
            QString      name;
            QString      description;

            // Route Id
            int          id = -1;

            // Direction of this route
            // Out: Speaker; In: Mic
            int          direction = -1;

            // Priority of this route
            // Decides if this route is used
            int          priority = -1;

            // I have no idea what this is
            int          device = -1;

            // Average volume: average( channelVolumes )
            qreal        volume = 1.0;

            // Audio channel volumes and their respective names
            QList<qreal> channelVolumes;
            QStringList  channelNames;

            // Mute
            bool         isMuted = false;

            // bool operator==( const RouteProperties& lhs, const RouteProperties& rhs );
        } RouteProperties;

        Q_SIGNAL void sinkPropertiesChanged( RouteProperties );
        Q_SIGNAL void sourcePropertiesChanged( RouteProperties );

    private:
        uint32_t mGlobalId     = 0;
        struct pw_device *mObj = nullptr;

        struct spa_hook *mListener             = nullptr;
        struct pw_device_events *mDeviceEvents = nullptr;

        Properties mProperties;

        bool mPendingProperties;
        bool mPendingParameters;

        RouteProperties mInputProps;
        RouteProperties mOutputProps;

        // For thread safety
        QMutex mMutex;

        static void deviceEventInfo( void *data, const struct pw_device_info *info );
        static void deviceEventParam( void *data, int seq, uint32_t id, uint32_t index, uint32_t next, const struct spa_pod *param );

        bool setRouteParameter( QString json );
};
