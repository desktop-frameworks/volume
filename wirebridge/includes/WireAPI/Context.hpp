/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>

#include "WireAPI/Base.hpp"

struct spa_dict;
struct spa_hook;
struct pw_context_events;
struct pw_context;
struct pw_global;
struct pw_impl_client;
struct pw_impl_node;

namespace DFL {
    namespace WireAPI {
        class MainLoop;
        class Context;
    }
}

class DFL::WireAPI::Context : public DFL::WireAPI::Base {
    Q_OBJECT;

    public:
        Context( const MainLoop& mainLoop, QObject *parent = nullptr );
        ~Context();

        // Funtion to inform the class that it can start relaying the events
        void setup() override;

        // Is this object valid
        bool isValid() const override;

        // Get the underlying raw pointer
        struct pw_context * pwContext();

        struct pw_context *pwContext() const;

        Q_SIGNAL void destroyed();
        Q_SIGNAL void freed();

        Q_SIGNAL void checkAccess( struct pw_impl_client * );
        Q_SIGNAL void globalAdded( struct pw_global * );
        Q_SIGNAL void globalRemoved( struct pw_global * );
        Q_SIGNAL void driverAdded( struct pw_impl_node * );
        Q_SIGNAL void driverRemoved( struct pw_impl_node * );

    private:
        // pw_context deleter
        struct ContextDeleter {
            // We need to check if the pointer is valid before destroying it
            ContextDeleter( Context *instance );

            // The operator that actually destroys the pw_context* object;
            void operator()( struct pw_context *pointer ) const;

            private:
                // The Context instance
                Context *contextInstance;
        };

        // This is the smart-pointer that hosts the struct pw_context pointer.
        std::unique_ptr<struct pw_context, ContextDeleter> mObj;

        // This is used to listen to the events of the main loop.
        static struct spa_hook mListener;
        static struct pw_context_events mEvents;

        // Pending flag to emit various events when setup() is called
        bool pendingDestroyed = false;
        bool pendingFreed     = false;
        struct pw_impl_client *pendingClient    = nullptr;
        struct pw_global *pendingAddedGlobal    = nullptr;
        struct pw_global *pendingRemovedGlobal  = nullptr;
        struct pw_impl_node *pendingAddedNode   = nullptr;
        struct pw_impl_node *pendingRemovedNode = nullptr;

    protected:
        // Event handler: destroy
        static void handleDestroy( void *data );

        // Event handler: free
        static void handleFree( void *data );

        // Event handler: check_access
        static void handleCheckAccess( void *data, struct pw_impl_client *client );

        // Event handler: global_added
        static void handleGlobalAdded( void *data, struct pw_global *global );

        // Event handler: global_removed
        static void handleGlobalRemoved( void *data, struct pw_global *global );

        // Event handler: driver_added
        static void handleDriverAdded( void *data, struct pw_impl_node *node );

        // Event handler: driver_removed
        static void handleDriverRemoved( void *data, struct pw_impl_node *node );
};
