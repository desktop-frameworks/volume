/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>
#include <QThread>

#include "WireAPI/Base.hpp"
#include "WireAPI/Context.hpp"

struct spa_dict;
struct spa_hook;
struct pw_core;
struct pw_core_events;
struct pw_core_info;

namespace DFL {
    namespace WireAPI {
        class Context;
        class Core;
    }
}

class DFL::WireAPI::Core : public DFL::WireAPI::Base {
    Q_OBJECT;

    public:
        Core( const Context& ctxt, QObject *parent = nullptr );
        ~Core();

        // Funtion to inform the class that it can start relaying the events
        void setup() override;

        // Is this object valid
        bool isValid() const override;

        // Get the underlying raw pointer
        struct pw_core * pwCore();

        // Get the underlying raw pointer
        struct pw_core *pwCore() const;

        Q_SIGNAL void info( const struct pw_core_info * );
        Q_SIGNAL void done( uint32_t, int );
        Q_SIGNAL void ping( uint32_t, int );
        Q_SIGNAL void error( uint32_t, int, int, QString );
        Q_SIGNAL void idRemoved( uint32_t );
        Q_SIGNAL void idBound( uint32_t, uint32_t );
        Q_SIGNAL void memoryAdded( uint32_t, uint32_t, int, uint32_t );
        Q_SIGNAL void memoryRemoved( uint32_t );
        Q_SIGNAL void boundProps( uint32_t, uint32_t, const struct spa_dict * );

    private:
        // pw_core deleter
        struct CoreDeleter {
            // We need to check if the pointer is valid before destroying it
            CoreDeleter( Core *instance );

            // The operator that actually destroys the pw_core* object;
            void operator()( struct pw_core *pointer ) const;

            private:
                // The Core instance
                Core *coreInstance;
        };

        // This is the smart-pointer that hosts the struct pw_core pointer.
        std::unique_ptr<struct pw_core, CoreDeleter> mObj;

        // This is used to listen to the events of the main loop.
        static struct spa_hook mListener;
        static struct pw_core_events mEvents;

        const struct pw_core_info *mPendingInfo      = nullptr;
        std::tuple<bool, uint32_t, int> mPendingDone = { false, 0, 0 };
        std::tuple<bool, uint32_t, int, int, QString> mPendingError = { false, 0, 0, 0, "" };
        std::tuple<bool, uint32_t> mPendingRemoveId          = { false, 0 };
        std::tuple<bool, uint32_t, uint32_t> mPendingBoundId = { false, 0, 0 };
        std::tuple<bool, uint32_t, uint32_t, int, uint32_t> mPendingAddMem = { false, 0, 0, 0, 0 };
        std::tuple<bool, uint32_t> mPendingRemoveMem = { false, 0 };
        std::tuple<bool, uint32_t, uint32_t, const struct spa_dict *> mPendingBoundProps = { false, 0, 0, nullptr };

    protected:
        // Emitted when bound and as response to pw_core_hello()
        static void handleInfo( void *data, const struct pw_core_info *info );

        // Emitted as a response to pw_core_sync()
        static void handleDone( void *data, uint32_t id, int seq );

        // Server send this: Respond with pw_core_pong()
        static void handlePing( void *data, uint32_t id, int seq );

        // Send when a fatal error occurs (eg. when pipewire server dies)
        static void handleError( void *data, uint32_t id, int seq, int res, const char *message );

        // Emitted when a client deletes an object
        static void handleRemoveId( void *data, uint32_t id );

        // Emitted when a client binds an id (emitted before the global object becomes visible)
        static void handleBoundId( void *data, uint32_t id, uint32_t global_id );

        // Emitted when client is goven some memory of certain type
        static void handleAddMem( void *data, uint32_t id, uint32_t type, int fd, uint32_t flags );

        // Emitted when the memory is freed
        static void handleRemoveMem( void *data, uint32_t id );

        // Emitted when a local id is bound to a global id
        static void handleBoundProps( void *data, uint32_t id, uint32_t global_id, const struct spa_dict *props );
};
