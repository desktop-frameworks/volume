/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QHash>
#include <QObject>

struct spa_support;
struct spa_dict;

namespace DFL {
    namespace WireAPI {
        // Typdef Properties to QHash<QString, QString>
        typedef QHash<QString, QString> Properties;

        /** Pipewire initialization */
        void initPW( int argc = 0, char *argv[] = nullptr );

        /** Pipewire deinitialization */
        void deinitPW();

        /** Get the name of the application that's calling this instance of pipewire */
        const QString getApplicationName();

        const QString getProgramName();

        /** Get the user name of this instance */
        const QString getUserName();

        /** Get the host name of this instance */
        const QString getHostName();

        const QString getClientName();

        bool checkOption( const QString& option, const QString& value );

        int directionReverse( int direction );

        const QString getDomain();
        int setDomain( const QString& domain );

        uint32_t getSupport( struct spa_support *support, uint32_t max_support );

        /** Convenience function to convert spa_dict to QHash */
        Properties convertSpaDictToMap( const struct spa_dict *dict );
    }
}
