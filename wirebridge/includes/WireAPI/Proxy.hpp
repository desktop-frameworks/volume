/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QHash>
#include <QObject>

#include "WireAPI/Pipewire.hpp"
#include "WireAPI/Base.hpp"

struct spa_dict;
struct spa_hook;
struct pw_proxy;
struct pw_proxy_events;
struct pw_proxy_events;

namespace DFL {
    namespace WireAPI {
        class Proxy;
    }
}

class DFL::WireAPI::Proxy : public DFL::WireAPI::Base {
    Q_OBJECT;

    public:
        Proxy( struct pw_proxy *proxy, QObject *parent = nullptr );
        ~Proxy();

        // For deferred initialization
        void initialize( struct pw_proxy * );

        // Funtion to inform the class that it can start relaying the events
        virtual void setup() override;

        // Is this object valid
        virtual bool isValid() const override;

        // Get the underlying raw pointer
        struct pw_proxy * pwProxy();

        // Get the underlying raw pointer
        struct pw_proxy *pwProxy() const;

        /**
         * Ask Pipewire to sync
         * Pipewire will respond to this event with the same seq.
         */
        void sync( int seq );

        /**
         * This proxy has been removed from the server
         * This event will always be followed by destroyed
         * Immediately this event is emitted, this proxy will be destroyed
         */
        Q_SIGNAL void removed();

        /** This proxy has been destroyed */
        Q_SIGNAL void destroyed();

        /** This proxy was bound to a global id. */
        Q_SIGNAL void bound( uint32_t );

        /**
         * Emitted in response to sync(...)
         * For each sync(...), a corresponding done(...) will be emitted.
         */
        Q_SIGNAL void done( int );

        /** An error occurred on this proxy */
        Q_SIGNAL void error( int seq, int res, const QString& message );

        /** Enhanced version of bound(...) */
        Q_SIGNAL void boundProps( uint32_t, const Properties& );

    private:
        // pw_proxy deleter
        struct ProxyDeleter {
            // We need to check if the pointer is valid before destroying it
            ProxyDeleter( Proxy *instance );

            // The operator that actually destroys the pw_proxy* object;
            void operator()( struct pw_proxy *pointer ) const;

            private:
                // The Proxy instance
                Proxy *proxyInstance;
        };

        // This is the smart-pointer that hosts the struct pw_proxy pointer.
        std::unique_ptr<struct pw_proxy, ProxyDeleter> mObj;

        // Pending global events
        bool mPendingRemoved   = false;
        bool mPendingDestroyed = false;
        uint32_t mPendingBound = 0;
        int mPendingDone       = 0;
        QVariantList mPendingError;
        QVariantList mPendingBoundProps;

        // This is used to listen to the events of the proxy.
        struct spa_hook *mListener      = nullptr;
        struct pw_proxy_events *mEvents = nullptr;

    protected:
        /** Handle the pw_proxy event destroy */
        static void handleDestroy( void *data );
        static void handleBound( void *data, uint32_t global_id );
        static void handleRemoved( void *data );
        static void handleDone( void *data, int seq );
        static void handleError( void *data, int seq, int res, const char *message );
        static void handleBoundProps( void *data, uint32_t global_id, const struct spa_dict *props );
};
