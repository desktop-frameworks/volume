/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>
#include <QMutex>
#include <QThread>

#include "WireAPI/Pipewire.hpp"
#include "WireAPI/Proxy.hpp"

struct pw_node;
struct pw_proxy;
struct spa_hook;
struct spa_pod;
struct pw_node_events;
struct pw_node_info;

namespace DFL {
    namespace WireAPI {
        class Registry;
        class Node;
    }
}

class DFL::WireAPI::Node : public DFL::WireAPI::Proxy {
    Q_OBJECT;

    public:
        enum class Type {
            Sink,
            Source,
            Stream,
        };

        enum class State {
            Error     = -1,
            Creating  = 0,
            Suspended = 1,
            Idle      = 2,
            Running   = 3,
        };

        Node( struct pw_node *, Properties props, uint32_t id, QObject *parent = nullptr );
        ~Node();

        void setup() override;

        struct pw_node *pwNode();

        struct pw_node *pwNode() const;

        uint32_t globalId();

        Properties properties();

        State state();
        QString error();

        /** Volume getter/setter */
        qreal volume();
        bool setVolume( qreal );

        /** Mute getter/setter */
        bool isMuted();
        bool setMuted( bool );

        Q_SIGNAL void stateChanged();

        Q_SIGNAL void parametersChanged();
        Q_SIGNAL void propertiesChanged();

    private:
        uint32_t mGlobalId   = 0;
        struct pw_node *mObj = nullptr;

        struct spa_hook *mListener     = nullptr;
        struct pw_node_events *mEvents = nullptr;

        Properties mProperties;

        State mState;
        QString mError;

        bool mPendingState;
        bool mPendingProperties;
        bool mPendingParameters;

        // For thread safety
        QMutex mMutex;

        static void nodeEventInfo( void *data, const struct pw_node_info *info );
        static void nodeEventParam( void *data, int seq, uint32_t id, uint32_t index, uint32_t next, const struct spa_pod *param );
};
