/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>
#include <QThread>

#include "WireAPI/Pipewire.hpp"
#include "WireAPI/Proxy.hpp"

struct pw_metadata;
struct pw_proxy;
struct spa_hook;
struct spa_pod;
struct pw_metadata_events;

namespace DFL {
    namespace WireAPI {
        class Registry;
        class Metadata;
    }
}

class DFL::WireAPI::Metadata : public DFL::WireAPI::Proxy {
    Q_OBJECT;

    public:
        Metadata( struct pw_metadata *, Properties props, uint32_t id, QObject *parent = nullptr );
        ~Metadata();

        void setup() override;

        struct pw_metadata *pwMetadata();

        struct pw_metadata *pwMetadata() const;

        // Name of this metadata object
        QString name() const;

        uint32_t globalId();

        Properties properties();
        void setProperty( uint32_t, QString, QString, QString );

        /** Clear all metadata */
        void clear();

        Q_SIGNAL void propertyChanged( uint32_t, QString, QString, QString );

    private:
        uint32_t mGlobalId       = 0;
        struct pw_metadata *mObj = nullptr;

        struct spa_hook *mListener         = nullptr;
        struct pw_metadata_events *mEvents = nullptr;

        Properties mProperties;
        QHash<QString, QVariantList> mAcquiredProps;
        QHash<QString, QVariantList> mPendingProps;

        static int handleProperty( void *data, uint32_t subject, const char *key, const char *type, const char *value );
};
