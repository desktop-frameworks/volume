/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QHash>
#include <QObject>

#include "WireAPI/Core.hpp"
#include "WireAPI/Proxy.hpp"

struct spa_dict;
struct spa_hook;
struct pw_registry;
struct pw_proxy_events;
struct pw_registry_events;

struct pw_device;
struct pw_node;
struct pw_port;
struct pw_metadata;

namespace DFL {
    namespace WireAPI {
        class Registry;
    }
}

class DFL::WireAPI::Registry : public DFL::WireAPI::Proxy {
    Q_OBJECT;

    public:
        Registry( const Core& core, QObject *parent = nullptr );
        ~Registry();

        // Funtion to inform the class that it can start relaying the events
        void setup() override;

        // Is this object valid
        bool isValid() const override;

        // Get the underlying raw pointer
        struct pw_registry * pwRegistry();

        // Get the underlying raw pointer
        struct pw_registry *pwRegistry() const;

        // Bind this id to pw_device
        struct pw_device * bindDevice( uint32_t id );

        // Bind this id to pw_node
        struct pw_node * bindNode( uint32_t id );

        // Bind this id to pw_port
        struct pw_port * bindPort( uint32_t id );

        // Bind this id to pw_metadata
        struct pw_metadata * bindMetadata( uint32_t id );

        // Bind this id to a generic global
        void * bindGlobal( uint32_t id, QString type, uint32_t version );

        Q_SIGNAL void deviceAdded( uint32_t id, uint32_t version, const DFL::WireAPI::Properties& props );
        Q_SIGNAL void nodeAdded( uint32_t id, uint32_t version, const DFL::WireAPI::Properties& props );
        Q_SIGNAL void portAdded( uint32_t id, uint32_t version, const DFL::WireAPI::Properties& props );
        Q_SIGNAL void metadataAdded( uint32_t id, uint32_t version, const DFL::WireAPI::Properties& props );
        Q_SIGNAL void globalAdded( uint32_t id, uint32_t version, QString type, const DFL::WireAPI::Properties& props );

        Q_SIGNAL void globalRemoved( uint32_t id );

    private:
        // We will use raw-pointers. The underlying pw_proxy is the smart pointer.
        struct pw_registry *mObj;

        // Pending global events
        QHash<uint32_t, QVariantList> mPending;

        // This is used to listen to the events of the registry.
        struct spa_hook *mListener;
        struct pw_registry_events *mEvents;

    protected:
        static void handleGlobal( void *data, uint32_t, uint32_t, const char *, uint32_t, const struct spa_dict * );
        static void handleGlobalRemove( void *, uint32_t );
};
