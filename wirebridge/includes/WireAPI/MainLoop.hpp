/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>

#include "WireAPI/Base.hpp"

struct spa_dict;
struct spa_hook;
struct pw_main_loop;
struct pw_loop;
struct pw_main_loop_events;

namespace DFL {
    namespace WireAPI {
        class MainLoop;
    }
}

class DFL::WireAPI::MainLoop : public DFL::WireAPI::Base {
    Q_OBJECT;

    public:
        MainLoop( QObject *parent = nullptr );
        ~MainLoop();

        // Funtion to inform the class that it can start relaying the events
        void setup() override;

        // Is this object valid
        bool isValid() const override;

        // Run the main loop.
        // This is a blocking function: it blocks untul quit() is called
        // A good way to unblock this, is to move to a thread:
        //    QThread thread;
        //    DFL::WireAPI::MainLoop *mainLoop = new DFL::WireAPI::MainLoop();
        //    connect( &thread, &QThread::started, &mainloop, &DFL::MainLoop:run );
        //    thread.start();
        int run();

        // Quit the main loop.
        int quit();

        // Get the underlying pw_loop *
        struct pw_loop *getPwLoop() const;

        // Get the underlying raw pointer
        struct pw_main_loop * pwMainLoop();

        struct pw_main_loop *pwMainLoop() const;

        Q_SIGNAL void destroyed();

    private:
        // pw_main_loop deleter
        struct MainloopDeleter {
            // We need to check if the pointer is valid before destroying it
            MainloopDeleter( MainLoop *instance );

            // The operator that actually destroys the pw_main_loop* object;
            void operator()( struct pw_main_loop *pointer ) const;

            private:
                // The MainLoop instance
                MainLoop *mainLoopInstance;
        };

        // This is the smart-pointer that hosts the struct pw_main_loop pointer.
        std::unique_ptr<struct pw_main_loop, MainloopDeleter> mObj;

        // This is used to listen to the events of the main loop.
        static struct spa_hook mListener;
        static struct pw_main_loop_events mEvents;

        // Pending flag to emit destroyed() when setup() is called
        bool mPending = false;

    protected:
        // Event handler: destroy
        static void handleDestroy( void *data );
};
