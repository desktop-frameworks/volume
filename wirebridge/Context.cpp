/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDebug>
#include "WireAPI/Context.hpp"
#include "WireAPI/MainLoop.hpp"

#include <pipewire/pipewire.h>

struct spa_hook          DFL::WireAPI::Context::mListener = {};
struct pw_context_events DFL::WireAPI::Context::mEvents   = {
    .version        = PW_VERSION_CONTEXT_EVENTS,
    .destroy        = &DFL::WireAPI::Context::handleDestroy,
    .free           = &DFL::WireAPI::Context::handleFree,
    .check_access   = &DFL::WireAPI::Context::handleCheckAccess,
    .global_added   = &DFL::WireAPI::Context::handleGlobalAdded,
    .global_removed = &DFL::WireAPI::Context::handleGlobalRemoved,
    .driver_added   = &DFL::WireAPI::Context::handleDriverAdded,
    .driver_removed = &DFL::WireAPI::Context::handleDriverRemoved,
};

DFL::WireAPI::Context::Context( const MainLoop& mainLoop, QObject *parent ) : DFL::WireAPI::Base( parent ), mObj( nullptr, ContextDeleter( this ) ) {
    // Get the underlying pw_main_loop from PWMainLoop
    struct pw_loop *pwLoop = mainLoop.getPwLoop();

    if ( !pwLoop ) {
        qCritical() << "Failed to retrieve pw_loop *from DFL::WireAPI::MainLoop";
        return;
    }

    // Create the pw_context
    struct pw_context *context = pw_context_new( pwLoop, nullptr, 0 );

    if ( !context ) {
        qCritical() << "Failed to create pw_context";
        return;
    }

    // Assign the context to the smart pointer
    mObj.reset( context );

    spa_zero( mListener );
    pw_context_add_listener( mObj.get(), &mListener, &mEvents, this );

    mIsValid = true;
}


DFL::WireAPI::Context::~Context() {
    spa_hook_remove( &mListener );
}


void DFL::WireAPI::Context::setup() {
    if ( mIsSetup == false ) {
        mIsSetup = true;

        // Emit freed first
        if ( pendingFreed ) {
            emit freed();
        }

        // Emit destroyed after
        if ( pendingDestroyed ) {
            emit destroyed();
        }

        if ( pendingClient != nullptr ) {
            emit checkAccess( pendingClient );
        }

        if ( pendingAddedGlobal != nullptr ) {
            emit globalAdded( pendingAddedGlobal );
        }

        if ( pendingRemovedGlobal != nullptr ) {
            emit globalRemoved( pendingRemovedGlobal );
        }

        if ( pendingAddedNode != nullptr ) {
            emit driverAdded( pendingAddedNode );
        }

        if ( pendingRemovedNode != nullptr ) {
            emit driverRemoved( pendingRemovedNode );
        }
    }
}


bool DFL::WireAPI::Context::isValid() const {
    return mIsValid;
}


struct pw_context * DFL::WireAPI::Context::pwContext() {
    return mObj.get();
}


struct pw_context * DFL::WireAPI::Context::pwContext() const {
    return mObj.get();
}


void DFL::WireAPI::Context::handleDestroy( void *data ) {
    DFL::WireAPI::Context *ctxt = reinterpret_cast<DFL::WireAPI::Context *>( data );

    ctxt->mObj.reset( nullptr );
    ctxt->mIsValid = false;

    if ( ctxt ) {
        if ( ctxt->mIsSetup ) {
            emit ctxt->destroyed();
        }

        else {
            ctxt->pendingDestroyed = true;
        }
    }
}


void DFL::WireAPI::Context::handleFree( void *data ) {
    DFL::WireAPI::Context *ctxt = reinterpret_cast<DFL::WireAPI::Context *>( data );

    ctxt->mObj.reset( nullptr );
    ctxt->mIsValid = false;

    if ( ctxt ) {
        if ( ctxt->mIsSetup ) {
            emit ctxt->freed();
            emit ctxt->destroyed();
        }

        else {
            ctxt->pendingDestroyed = true;
            ctxt->pendingFreed     = true;
        }
    }
}


void DFL::WireAPI::Context::handleCheckAccess( void *data, struct pw_impl_client *client ) {
    DFL::WireAPI::Context *ctxt = reinterpret_cast<DFL::WireAPI::Context *>( data );

    if ( ctxt ) {
        if ( ctxt->mIsSetup ) {
            emit ctxt->checkAccess( client );
        }

        else {
            ctxt->pendingClient = client;
        }
    }
}


void DFL::WireAPI::Context::handleGlobalAdded( void *data, struct pw_global *global ) {
    DFL::WireAPI::Context *ctxt = reinterpret_cast<DFL::WireAPI::Context *>( data );

    if ( ctxt ) {
        if ( ctxt->mIsSetup ) {
            emit ctxt->globalAdded( global );
        }

        else {
            ctxt->pendingAddedGlobal = global;
        }
    }
}


void DFL::WireAPI::Context::handleGlobalRemoved( void *data, struct pw_global *global ) {
    DFL::WireAPI::Context *ctxt = reinterpret_cast<DFL::WireAPI::Context *>( data );

    if ( ctxt ) {
        if ( ctxt->mIsSetup ) {
            emit ctxt->globalRemoved( global );
        }

        else {
            ctxt->pendingRemovedGlobal = global;
        }
    }
}


void DFL::WireAPI::Context::handleDriverAdded( void *data, struct pw_impl_node *node ) {
    DFL::WireAPI::Context *ctxt = reinterpret_cast<DFL::WireAPI::Context *>( data );

    if ( ctxt ) {
        if ( ctxt->mIsSetup ) {
            emit ctxt->driverAdded( node );
        }

        else {
            ctxt->pendingAddedNode = node;
        }
    }
}


void DFL::WireAPI::Context::handleDriverRemoved( void *data, struct pw_impl_node *node ) {
    DFL::WireAPI::Context *ctxt = reinterpret_cast<DFL::WireAPI::Context *>( data );

    if ( ctxt ) {
        if ( ctxt->mIsSetup ) {
            emit ctxt->driverRemoved( node );
        }

        else {
            ctxt->pendingRemovedNode = node;
        }
    }
}


/**
 * Deleter Struct
 */

DFL::WireAPI::Context::ContextDeleter::ContextDeleter( DFL::WireAPI::Context *instance ) : contextInstance( instance ) {
    // Nothing else to do
}


void DFL::WireAPI::Context::ContextDeleter::operator()( struct pw_context *pointer ) const {
    // Call the destructor only if the pointer is valid.
    if ( contextInstance->isValid() ) {
        // Perform the destruction
        pw_context_destroy( pointer );

        // Set the pointer to nullptr
        pointer = nullptr;
    }
}
